import React from 'react';
import { ThemeProvider } from 'styled-components';
import { ConfigProvider, Empty } from 'antd';
import theme from 'configs/theme';
import Routes from 'routes';
import 'configs/language';
import GlobalStyle from 'styles';
import firebaseInit from 'configs/firebase';
import withRedux from 'hocs/withRedux';

firebaseInit();

const App = () => (
  <ThemeProvider theme={theme}>
    <ConfigProvider renderEmpty={() => <Empty />}>
      <GlobalStyle />
      <Routes />
    </ConfigProvider>
  </ThemeProvider>
);

export default withRedux(App);
