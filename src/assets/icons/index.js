import enouvo from './enouvo.svg';
import facebook from './facebook.svg';
import mail from './mail.svg';
import instagram from './instagram.svg';
import behance from './behance.svg';
import linkedin from './linkedin.svg';
import location from './location.svg';
import phone from './phone.svg';
import path from './path.svg';
import cross from './cross.svg';
import { ReactComponent as GoogleIcon } from './google.svg';
import { ReactComponent as HamburgerIcon } from './hamburger.svg';
import cancel from './cancel.svg';
import { ReactComponent as NotFoundIcon } from './404.svg';
import { ReactComponent as UserIcon } from './user.svg';
import { ReactComponent as PreviewIcon } from './preview.svg';
import { ReactComponent as ContactIcon } from './contact.svg';
import { ReactComponent as SourceIcon } from './source.svg';
import { ReactComponent as CalendarIcon } from './calendar.svg';
import { ReactComponent as BriefCaseIcon } from './briefcase.svg';
import { ReactComponent as EmployeeIcon } from './employee.svg';
import { ReactComponent as ExcelButtonIcon } from './excelButton.svg';

export {
  cross,
  enouvo,
  facebook,
  mail,
  instagram,
  behance,
  linkedin,
  location,
  phone,
  path,
  GoogleIcon,
  HamburgerIcon,
  cancel,
  NotFoundIcon,
  UserIcon,
  PreviewIcon,
  ContactIcon,
  SourceIcon,
  CalendarIcon,
  BriefCaseIcon,
  EmployeeIcon,
  ExcelButtonIcon,
};
