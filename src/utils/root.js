const freeGlobal =
  typeof global === 'object' &&
  global !== null &&
  global.Object === Object &&
  global;

// eslint-disable-next-line no-redeclare
/* global globalThis, self */

/** Detect free variable `globalThis` */
const freeGlobalThis =
  typeof globalThis === 'object' &&
  globalThis !== null &&
  // eslint-disable-next-line eqeqeq
  globalThis.Object == Object &&
  globalThis;

/** Detect free variable `self`. */
const freeSelf =
  // eslint-disable-next-line no-restricted-globals
  typeof self === 'object' && self !== null && self.Object === Object && self;

/** Used as a reference to the global object. */
const root =
  // eslint-disable-next-line no-new-func
  freeGlobalThis || freeGlobal || freeSelf || Function('return this')();

export default root;
