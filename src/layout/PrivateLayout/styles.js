import styled from 'styled-components';
import { Layout } from 'antd';

const { Content } = Layout;

export const StyledPrivateLayout = styled(Layout)`
  height: 100vh;
  .ant-table {
    font-size: 1em;
  }

  .content {
  }

  .triggerMobile {
    font-size: 20px;
    line-height: 64px;
    cursor: pointer;
    color: ${({ theme }) => theme.palette.primary};
    transition: color 0.3s;
    position: fixed;
    top: 0px;
    left: 20px;
    z-index: 2;
    display: none;
    &:hover {
      color: ${({ theme }) => theme.palette.primary};
    }
    @media only screen and (max-width: 430px) {
      display: block;
    }
  }
  .ant-layout-header {
    line-height: 64px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  }
  .mainContent {
    min-height: 280;
  }

  .footer {
    text-align: center;
    @media only screen and (max-width: 430px) {
      display: none;
    }
  }

  .footerMobile {
    position: fixed;
    height: 60px;
    left: 0px;
    right: 0px;
    bottom: -60px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: #fff;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.15);
    transition: all 0.5s ease 0.2s;
    a {
      text-align: center;
      flex: 1;
    }
    .tabIcon {
      font-size: 25px;
    }
    @media only screen and (max-width: 430px) {
      bottom: 0px;
    }
  }
`;

export const StyledMainView = styled(Layout)`
  height: 100vh;
  overflow: hidden;
  transition: all 0.3s ease 0s;
  background: #eff3f5;
  @media only screen and (max-width: 430px) {
    margin-left: 0px;
    z-index: 1;
  }
`;

export const StyledContent = styled(Content)`
  padding: 30px 30px 40px 40px;
  flex: 1;
`;
