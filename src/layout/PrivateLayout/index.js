import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { useLocation } from 'react-router';
import { StyledPrivateLayout, StyledMainView, StyledContent } from './styles';
import { CustomHeader, CustomSlider } from '../../components/common';
// eslint-disable-next-line import/no-cycle
import { PUBLIC_ROUTES } from '../../routes/PublicRoutes';
import { useBreakpoint } from '../../hooks';

const PrivateLayout = ({ isAuthenticated, children }) => {
  const breakpoint = useBreakpoint();
  const location = useLocation();
  if (!PUBLIC_ROUTES.find(e => location.pathname.search(e.path) > -1)) {
    return !isAuthenticated ? (
      <Redirect to="/login" />
    ) : (
      <StyledPrivateLayout>
        {breakpoint !== 'xs' && <CustomSlider />}
        <StyledMainView>
          <CustomHeader />
          <StyledContent>{children}</StyledContent>
        </StyledMainView>
      </StyledPrivateLayout>
    );
  }
  return null;
};

PrivateLayout.propTypes = {
  children: PropTypes.node.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

export default connect(state => ({
  isAuthenticated: state.auth.isAuthenticated,
  isLoading: state.loading.isLoading,
}))(PrivateLayout);
