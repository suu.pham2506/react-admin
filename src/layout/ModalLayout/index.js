import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CustomModal from '../../components/common/Modal';
import { MODAL_LISTS } from '../../routes/PrivateRoutes';

import { closeModalAction, toggleModalAction } from '../../redux/modal/actions';

const ModalLayout = ({ closeModal, toggleModal, modals }) => {
  const getCurrentModalList = current => {
    const modalList =
      current && MODAL_LISTS.find(route => current.search(route.path) > -1);
    if (modalList) {
      return modalList.modals.find(route => current.indexOf(route.path) > -1);
    }
    return modalList;
  };
  const handleCloseModal = id => {
    closeModal(id);
  };
  const modalCurrentLists = modals.map(modal =>
    getCurrentModalList(modal.current),
  );
  return (
    <div className="wrapper">
      {modalCurrentLists.map((modal, index) => (
        <CustomModal
          {...{ toggleModal }}
          isHidden={modals[index].isHidden}
          onCancel={() => handleCloseModal(modals[index].id)}
          title={modal ? modal.title : null}
          key={modals[index].id}
          id={modals[index].id}
        >
          {modal && modal.component && !modals[index].isHidden && (
            <modal.component id={modals[index].id} showModal />
          )}
        </CustomModal>
      ))}
    </div>
  );
};

ModalLayout.propTypes = {
  closeModal: PropTypes.func,
  toggleModal: PropTypes.func,
  modals: PropTypes.array,
};

export default connect(
  state => ({
    location: state.router.location,
    modals: state.modal.modals,
  }),
  dispatch => ({
    closeModal: id => dispatch(closeModalAction(id)),
    toggleModal: id => dispatch(toggleModalAction(id)),
  }),
)(ModalLayout);
