import styled from 'styled-components';

const primaryFont = props => props.theme.fonts.primary;

export default styled.div`
  font-family: ${primaryFont}, sans-serif;
  .layout {
    z-index: 0;
    position: relative;
    width: 100%;
    background-color: #fff;
  }
  .main-content {
    font-size: 16px;
    width: 100%;
    @media all and (max-width: 992px) and (min-width: 768px) {
      font-size: 90%;
    }
    @media all and (max-width: 768px) {
      font-size: 80%;
    }
  }
`;
