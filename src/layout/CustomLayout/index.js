import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import { useSelector } from 'react-redux';
import CustomLayoutWrapper from './styles';
import LoadingComponent from '../../components/common/LoadingScreenTransparent';

const CustomLayout = ({ children }) => {
  const isLoading = useSelector(state => state.loading.isLoading);
  return (
    <CustomLayoutWrapper>
      {isLoading && <LoadingComponent />}
      <div className="background" />
      <Layout className="layout">
        <div className="main-content">{children}</div>
      </Layout>
    </CustomLayoutWrapper>
  );
};

CustomLayout.propTypes = {
  children: PropTypes.any,
};

export default CustomLayout;
