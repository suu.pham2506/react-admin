import Create from './Create';
import Edit from './Edit';
import List from '../../containers/PositionTable';

export default { Create, Edit, List };
