import React from 'react';
// import PropTypes from 'prop-types';
import CustomLayout from 'layout/CustomLayout';
import InterviewFormContainer from '../../containers/InterviewForm';

function InterViewForm() {
  return (
    <CustomLayout>
      <InterviewFormContainer />
    </CustomLayout>
  );
}

InterViewForm.propTypes = {};

export default InterViewForm;
