import Create from './Create';
import Edit from './Edit';
import List from '../../containers/QuestionsTable';

export default { Create, Edit, List };
