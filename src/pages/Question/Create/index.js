import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import { connect } from 'react-redux';
import { itemSelector, draftSelector } from 'redux/modal/selectors';
import { addQuestionAction } from 'redux/previewInterview/actions';
import { closeModalAction, saveDraftAction } from 'redux/modal/actions';

const { TextArea } = Input;

const Create = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  addQuestion,
  closeModal,
  match,
  id,
  draft,
  saveDraft,
}) => {
  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        addQuestion(match.params.id, values);
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const value = getFieldsValue();
      saveDraft({ data: value, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item label="Type" colon={false}>
        {getFieldDecorator('type', {
          rules: [{ required: true, message: 'Please input your username!' }],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Text" colon={false}>
        {getFieldDecorator('text', {
          rules: [{ required: true, message: 'Please input your username!' }],
        })(<TextArea autoSize={{ minRows: 1 }} />)}
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Confirm
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.object.isRequired,
  addQuestion: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
};

export default connect(
  (state, props) => ({
    match: itemSelector(state, props),
    draft: draftSelector(state, props),
  }),
  dispatch => ({
    addQuestion: (id, data) => dispatch(addQuestionAction(id, data)),
    closeModal: id => dispatch(closeModalAction(id)),
    saveDraft: params => dispatch(saveDraftAction(params)),
  }),
)(Form.create()(Create));
