import Create from './Create';
import Edit from './Edit';
import List from '../../containers/AnswerTable';

export default { Create, Edit, List };
