import styled from 'styled-components';

export default styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .loginHeader {
    color: ${props => props.theme.palette.primary};
    font-family: ${props => props.theme.fonts.secondaryBold};
    font-size: 64px;
    margin-bottom: 10%;
  }
`;
