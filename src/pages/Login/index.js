import React from 'react';
import LoginForm from 'containers/Login';
import PublicLayout from 'layout/PublicLayout';
import LoginWrapper from './styles';

export default function Login() {
  return (
    <PublicLayout>
      <LoginWrapper>
        <LoginForm />
      </LoginWrapper>
    </PublicLayout>
  );
}
