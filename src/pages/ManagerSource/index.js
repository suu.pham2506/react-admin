import Create from './Create';
import Edit from './Edit';
import List from '../../containers/ManagerSourceTable';

export default { Create, Edit, List };
