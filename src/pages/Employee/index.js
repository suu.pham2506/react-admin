import Create from './Create';
import Edit from './Edit';
import { EmployeeTable as List } from '../../containers';

export default { Create, Edit, List };
