import React, { useEffect } from 'react';
import { Form, Button, Input } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import { createUserAction } from '../../../redux/users/actions';

const Create = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  createUser,
  closeModal,
  id,
  saveDraft,
  draft,
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        createUser({ user: values });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={onSubmit}>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Role" colon={false}>
        {getFieldDecorator('role', {
          rules: [{ required: true, message: 'Please enter role' }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  createUser: PropTypes.func,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  id: PropTypes.number,
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  createUser: params => dispatch(createUserAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Create));
