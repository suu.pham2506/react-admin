import Create from './Create';
import Edit from './Edit';
import List from '../../containers/UserTable';

export default { Create, Edit, List };
