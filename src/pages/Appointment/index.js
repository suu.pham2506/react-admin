import Create from './Create';
import Edit from './Edit';
import List from '../../containers/AppointmentTable';

export default { Create, Edit, List };
