import React, { useEffect } from 'react';
import {
  Form,
  Button,
  Input,
  DatePicker,
  TimePicker,
  Col,
  Row,
  Select,
} from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { createAppointmentAction } from '../../../redux/appointments/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import StyledForm from './style';
import { mergedDateAndTime } from '../../../utils/timeUtils';
import { getAllContactAction } from '../../../redux/contact/actions';

const { Option } = Select;

const Create = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  createAppointment,
  closeModal,
  id,
  draft,
  saveDraft,
  contacts,
  getAllContact,
}) => {
  const handSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const startTime = mergedDateAndTime(
          values.startDate,
          values.startTime,
        ).getTime();
        const endTime = mergedDateAndTime(
          values.endDate,
          values.endTime,
        ).getTime();
        createAppointment({
          appointment: {
            startTime,
            endTime,
            location: values.location,
            attendees: values.attendees,
            title: values.title,
          },
        });
        closeModal(id);
      }
    });
  };
  const handleChangeEndDate = e => {
    setFieldsValue({ endDate: e });
  };
  const handleChangeEndTime = e => {
    const newEndTime = moment(new Date(e).getTime() + 60 * 60 * 1000);
    setFieldsValue({ endTime: newEndTime });
  };
  useEffect(() => {
    getAllContact();
  }, [getAllContact]);

  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <StyledForm>
      <Form onSubmit={handSubmit} className="login-form">
        <Form.Item label="Title" colon={false}>
          {getFieldDecorator('title', {
            rules: [{ required: true, message: i18n.t('messages.title') }],
          })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
        </Form.Item>
        <Form.Item label="Location" colon={false}>
          {getFieldDecorator('location', {
            rules: [{ required: true, message: i18n.t('messages.location') }],
          })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
        </Form.Item>
        <Row>
          <Col span={8}>
            <Form.Item label="Start Date" colon={false}>
              {getFieldDecorator('startDate', {
                rules: [
                  {
                    required: true,
                    message: i18n.t('messages.date'),
                  },
                ],
              })(<DatePicker onChange={e => handleChangeEndDate(e)} />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Start Time" colon={false}>
              {getFieldDecorator('startTime', {
                rules: [
                  {
                    required: true,
                    message: i18n.t('messages.time'),
                  },
                ],
              })(<TimePicker onChange={e => handleChangeEndTime(e)} />)}
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item label="End Date" colon={false}>
              {getFieldDecorator('endDate', {
                rules: [
                  {
                    required: true,
                    message: i18n.t('messages.date'),
                  },
                ],
              })(<DatePicker />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="EndTime" colon={false}>
              {getFieldDecorator('endTime', {
                rules: [
                  {
                    required: true,
                    message: i18n.t('messages.time'),
                  },
                ],
              })(<TimePicker />)}
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label="Attenddees">
          {getFieldDecorator('attendees', {
            rules: [
              {
                required: true,
                message: 'Please select your attendees!',
                type: 'array',
              },
            ],
          })(
            <Select mode="multiple">
              {contacts.map(contact => (
                <Option key={contact.id} value={contact.data.email}>
                  {contact.data.email}
                </Option>
              ))}
            </Select>,
          )}
        </Form.Item>

        <Form.Item>
          <Button onClick={() => closeModal(id)}>
            {i18n.t('button.cancel')}
          </Button>
          <Button type="primary" htmlType="submit">
            {i18n.t('button.create')}
          </Button>
        </Form.Item>
      </Form>
    </StyledForm>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  createAppointment: PropTypes.func.isRequired,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  contacts: PropTypes.array,
  getAllContact: PropTypes.func.isRequired,
};

Create.defaultProps = {
  contacts: [],
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
  contacts: state.contact.contacts,
});

const mapDispatchToProps = dispatch => ({
  getAllContact: () => dispatch(getAllContactAction()),
  closeModal: id => dispatch(closeModalAction(id)),
  createAppointment: params => dispatch(createAppointmentAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Create));
