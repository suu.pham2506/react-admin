import styled from 'styled-components';

export default styled.div`
  .ant-form .ant-row .ant-col .ant-form-item {
    margin-top: 0px;
    margin-bottom: 10px;
    flex-direction: column;
    .ant-form-item-label {
      text-align: left;
    }
  }
  .ant-select-selection--multiple {
    min-height: 40px;
    background: #eef0f1;
    padding-bottom: 3px;
    cursor: text;
    height: auto;
    border-radius: 2px;
    zoom: 1;
  }
  input#attendees {
    height: 25px;
    width: 100%;
  }
  .ant-select-selection--multiple > ul > li,
  .ant-select-selection--multiple .ant-select-selection__rendered > ul > li {
    margin-top: 7px;
  }
  .ant-calendar-picker {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    width: 128px;
    color: rgba(0, 0, 0, 0.65);
    font-size: 14px;
    font-variant: tabular-nums;
    line-height: 1.5;
    list-style: none;
    -webkit-font-feature-settings: 'tnum', 'tnum';
    font-feature-settings: 'tnum', 'tnum';
    position: relative;
    display: inline-block;
    outline: none;
    cursor: text;
    -webkit-transition: opacity 0.3s;
    transition: opacity 0.3s;
  }
`;
