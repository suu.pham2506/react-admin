import React, { useEffect } from 'react';
import { Form, Button, Input } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { editContactAction } from '../../../redux/contact/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editContact,
  closeModal,
  id,
  draft,
  saveDraft,
}) => {
  const handSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        editContact({ id: recordData.id, contact: values });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={handSubmit} className="login-form">
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator(
          'name',
          {
            initialValue: recordData.name || null,
          },
          {
            rules: [{ required: true, message: i18n.t('message.name') }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator(
          'email',
          {
            initialValue: recordData.email || null,
          },
          {
            rules: [{ required: true, message: i18n.t('message.email') }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Phone Number" colon={false}>
        {getFieldDecorator(
          'phone',
          {
            initialValue: recordData.phone || null,
          },
          {
            rules: [{ required: true, message: i18n.t('message.phone') }],
          },
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>Cancel</Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editContact: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  recordData: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  editContact: params => dispatch(editContactAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form.create()(Edit));
