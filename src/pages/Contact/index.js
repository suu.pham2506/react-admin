import Create from './Create';
import Edit from './Edit';
import List from '../../containers/ContactTable';

export default { Create, Edit, List };
