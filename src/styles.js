import { createGlobalStyle } from 'styled-components';

const fontPrimary = props => props.theme.fonts.secondary;

export default createGlobalStyle`
  html,
  body {
    font-family: ${fontPrimary},sans-serif !important;
    font-size: 100%;
    scroll-behavior: smooth;
  }
  .wrapper {
    position: fixed;
    z-index: 1000;
    display: flex;
    bottom: 0;
    right: 30px;
    top: auto;
    left: auto;
    align-items: flex-end;
    height: 0px;
  }
`;
