/* eslint-disable import/no-cycle */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ModalLayout from '../../layout/ModalLayout';
import { PageContent } from '../../components/common';

import {
  Appointment,
  Contact,
  PreviewInterview,
  Question,
  User,
  Position,
  ManagerSource,
  Employee,
  Answer,
  PrintAnswer,
} from '../../pages';
import PrivateLayout from '../../layout/PrivateLayout';

import {
  PreviewIcon,
  UserIcon,
  SourceIcon,
  ContactIcon,
  CalendarIcon,
  BriefCaseIcon,
  EmployeeIcon,
} from '../../assets/icons';

export const PRIVATE_ROUTES = [
  {
    key: 'user',
    text: 'Users',
    exact: true,
    icon: UserIcon,
    path: '/',
    routes: [
      {
        path: '/',
        title: 'user.title',
        component: User.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: User.Create,
        title: 'user.modals.create',
      },
      {
        path: '/edit',
        component: User.Edit,
        title: 'user.modals.edit',
      },
    ],
  },
  {
    path: '/preview-interview',
    exact: true,
    text: 'Preview',
    key: 'preview-interview',
    icon: PreviewIcon,
    routes: [
      {
        path: '/',
        title: 'previewInterview.title',
        component: PreviewInterview.List,
      },
      {
        title: 'question.title',
        path: '/:id',
        component: Question.List,
      },
    ],
    modals: [
      {
        path: '/question/edit',
        component: Question.Edit,
        title: 'question.modals.edit',
      },
      {
        path: '/:id/create',
        component: Question.Create,
        title: 'question.modals.create',
      },
      {
        path: '/create',
        component: PreviewInterview.Create,
        title: 'previewInterview.modals.create',
      },
    ],
  },
  {
    key: 'manager-source',
    exact: true,
    text: 'Source',
    icon: SourceIcon,
    path: '/manager-source',
    routes: [
      {
        path: '/',
        title: 'source.title',
        component: ManagerSource.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: ManagerSource.Create,
        title: 'source.modals.create',
      },
      {
        path: '/edit',
        component: ManagerSource.Edit,
        title: 'source.modals.edit',
      },
    ],
  },
  {
    key: 'contact',
    exact: true,
    text: 'Contact',
    icon: ContactIcon,
    path: '/contact',
    routes: [
      {
        path: '/',
        title: 'contact.title',
        component: Contact.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Contact.Create,
        title: 'contact.modals.create',
      },
      {
        path: '/edit',
        component: Contact.Edit,
        title: 'contact.modals.edit',
      },
    ],
  },
  {
    key: 'appointments',
    exact: true,
    text: 'Appointments',
    icon: CalendarIcon,
    path: '/appointments',
    routes: [
      {
        path: '/',
        title: 'appointments.title',
        component: Appointment.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Appointment.Create,
        title: 'appointments.modals.create',
      },
      {
        path: '/edit',
        component: Appointment.Edit,
        title: 'appointments.modals.edit',
      },
    ],
  },
  {
    key: 'positions',
    exact: true,
    text: 'Positions',
    icon: BriefCaseIcon,
    path: '/positions',
    routes: [
      {
        path: '/',
        title: 'positions.title',
        component: Position.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Position.Create,
        title: 'positions.modals.create',
      },
      {
        path: '/edit',
        component: Position.Edit,
        title: 'positions.modals.edit',
      },
    ],
  },
  {
    key: 'employees',
    exact: true,
    text: 'Employees',
    icon: EmployeeIcon,
    path: '/employees',
    routes: [
      {
        path: '/',
        title: 'employee.title',
        component: Employee.List,
      },
    ],
    modals: [
      {
        path: '/create',
        component: Employee.Create,
        title: 'employee.modals.create',
      },
      {
        path: '/edit',
        component: Employee.Edit,
        title: 'employee.modals.edit',
      },
    ],
  },
  {
    key: 'answers',
    exact: true,
    text: 'Answers',
    icon: SourceIcon,
    path: '/answers',
    routes: [
      {
        path: '/',
        title: 'answer.title',
        component: Answer.List,
      },
      {
        path: '/:id',
        title: 'answer.title',
        component: PrintAnswer.List,
      },
    ],
  },
];

export const MODAL_LISTS = [...PRIVATE_ROUTES].reverse(({ path, modals }) => ({
  path,
  modals,
}));

const PrivateRoutes = () => (
  <>
    <ModalLayout />
    <Switch>
      {PRIVATE_ROUTES.flatMap(
        route =>
          route.routes &&
          route.routes.map(subRoute => ({
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/',
          })),
      ).map(route => (
        <Route {...route} key={route.path}>
          <PrivateLayout>
            <PageContent title={route.title}>
              <route.component />
            </PageContent>
          </PrivateLayout>
        </Route>
      ))}
    </Switch>
  </>
);

export default PrivateRoutes;
