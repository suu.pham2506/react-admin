import React, { lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';

const InterviewForm = lazy(() => import('../../pages/InterViewForm'));
const Login = lazy(() => import('../../pages/Login'));
const NotFoundPage = lazy(() => import('../../pages/NotFoundPage'));

export const PUBLIC_ROUTES = [
  {
    path: '/interview-form',
    routes: [
      {
        path: '/:id',
        component: InterviewForm,
      },
    ],
  },
  {
    path: '/login',
    component: Login,
  },
];

const PublicRoutes = () => (
  <Switch>
    <Suspense fallback={<div>Loading...</div>}>
      {PUBLIC_ROUTES.flatMap(route =>
        route.routes
          ? route.routes.map(subRoute => ({
              ...subRoute,
              path: route.path + subRoute.path,
              exact: subRoute.path === '/',
            }))
          : route,
      ).map(route => (
        <Route {...route} key={route.path} />
      ))}
      <Route component={NotFoundPage} />
    </Suspense>
  </Switch>
);

export default PublicRoutes;
