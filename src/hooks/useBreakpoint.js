import { useState, useEffect } from 'react';
import throttle from '../utils/throttle';

const getDeviceConfig = width => {
  if (width < 576) {
    return 'xs';
  }
  if (width >= 576 && width < 720) {
    return 'sm';
  }
  if (width >= 720 && width < 992) {
    return 'md';
  }
  if (width >= 992 && width < 1200) {
    return 'lg';
  }
  if (width >= 1200) {
    return 'xl';
  }
};

const useBreakpoint = () => {
  const [brkPnt, setBrkPnt] = useState(() =>
    getDeviceConfig(window.innerWidth),
  );

  useEffect(() => {
    const calcInnerWidth = throttle(() => {
      setBrkPnt(getDeviceConfig(window.innerWidth));
    }, 200);
    window.addEventListener('resize', calcInnerWidth);
    return () => window.removeEventListener('resize', calcInnerWidth);
  }, []);

  return brkPnt;
};
export default useBreakpoint;

// ex: https://stackblitz.com/edit/usebreakpoint?file=useBreakpoint.js
