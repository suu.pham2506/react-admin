const THEME = {
  palette: {
    primary: '#1373ac',
    secondary: ['rgba(45, 48, 71, 1)', '#071924'],
    background: ['#fff'],
    loadingBackgroundColor: '#2c3e51cc',
    color: ['#788195', '#E4E6E9'],
    header: {
      primary: '#212629',
      button: '#198ace',
    },
    formSecondaryColor: 'rgba(193, 53, 31, 0.55)',
    formPrimaryColor: '#c1351f',
    formDarkColor: '#3a393d',
  },
  fonts: {
    primary: 'Poppins-Regular',
    primaryMedium: 'Poppins-Medium',
    primaryBold: 'Poppins-Bold',
    secondary: 'Barlow-Regular',
    secondaryMedium: 'Barlow-Medium',
    secondaryBold: 'Barlow-SemiBold',
  },
  fontWeight: {
    thin: 100, // Thin
    utraLight: 200, // Ultra Light
    light: 300, // Light
    regular: 400, // Regular
    medium: 500, // Medium
    semibold: 600, // Semibold
    bold: 700, // Bold
    heavy: 800, // Heavy
    black: 900, // Black
  },
  background: {
    container: '#f7f8fb',
    content: '#fff',
    input: '#efeff0',
    disabled: '#969696',
  },
  text: {
    primary: '#262626',
    secondary: '#969696',
    tabTitle: '#262626',
    empty: '#969696',
    highlight: '#1373ac',
    disabled: '#969696',
    formLabel: '#757575',
  },
  border: {
    default: '#d1d1d1',
  },
  scrollbar: {
    thumb: '',
    track: '',
  },
  color: {
    gray: '#a3a3a3',
  },
  alert: {
    error: 'red',
  },
};

module.exports = THEME;
