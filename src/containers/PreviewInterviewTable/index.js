import React, { useEffect } from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  getAllPreviewInterviewAction,
  deletePreviewInterviewAction,
  getNextPreviewInterviewsAction,
  getPrevPreviewInterviewsAction,
  unSubscribeAction,
} from '../../redux/previewInterview/actions';
import {
  lastInterviewSelector,
  firstInterviewSelector,
} from '../../redux/previewInterview/selectors';
import CustomTable from '../../components/common/CustomTable';

const PreviewInterviewTable = ({
  previewInterviews,
  getAllPreviewInterview,
  deletePreviewInterview,
  firstInterview,
  lastInterview,
  disableNext,
  disablePrev,
  getNextPreviewInterviews,
  getPreviousPreviewInterviews,
  unSubscribe,
}) => {
  useEffect(() => {
    getAllPreviewInterview();
    return () => {
      unSubscribe();
    };
  }, [getAllPreviewInterview, unSubscribe]);
  const handleDelete = id => {
    deletePreviewInterview(id);
  };

  const actionGroup = row => (
    <>
      <Link to={`/preview-interview/${row.id}`}>
        <Button type="primary">Edit</Button>
      </Link>
      <Button type="primary" onClick={() => handleDelete(row.id)}>
        Delete
      </Button>
    </>
  );
  const columns = [
    {
      title: 'Title',
      dataIndex: 'data.title',
      key: 'data.title',
      render: (text, row) => (
        <Link to={`/preview-interview/${row.id}`}>{text}</Link>
      ),
    },
    {
      render: (text, row) => actionGroup(row),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstInterview}
        lastData={lastInterview}
        {...{
          disableNext,
          disablePrev,
          columns,
        }}
        getPreviousData={() => getPreviousPreviewInterviews({ firstInterview })}
        getNextData={() => getNextPreviewInterviews({ lastInterview })}
        dataSource={previewInterviews}
        rowKey={record => record.id}
      />
    </>
  );
};

PreviewInterviewTable.propTypes = {
  previewInterviews: PropTypes.array,
  deletePreviewInterview: PropTypes.func,
  getAllPreviewInterview: PropTypes.func.isRequired,
  firstInterview: PropTypes.object,
  lastInterview: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  getNextPreviewInterviews: PropTypes.func,
  getPreviousPreviewInterviews: PropTypes.func,
  unSubscribe: PropTypes.func,
};

export default connect(
  state => ({
    previewInterviews: state.previewInterview.previewInterviews,
    firstInterview: firstInterviewSelector(state),
    lastInterview: lastInterviewSelector(state),
    disableNext: state.pagingButtons.disableNext,
    disablePrev: state.pagingButtons.disablePrev,
  }),
  dispatch => ({
    getAllPreviewInterview: () => dispatch(getAllPreviewInterviewAction()),
    deletePreviewInterview: id => dispatch(deletePreviewInterviewAction(id)),
    getNextPreviewInterviews: params =>
      dispatch(getNextPreviewInterviewsAction(params)),
    getPreviousPreviewInterviews: params =>
      dispatch(getPrevPreviewInterviewsAction(params)),
    unSubscribe: () => dispatch(unSubscribeAction()),
  }),
)(PreviewInterviewTable);
