import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import AnswerPage from '../../components/AnswerForm';
import { getAnswerByIdAction } from '../../redux/answers/actions';

const PrintAnswer = ({ getAnswerById, answer }) => {
  const params = useParams();
  useEffect(() => {
    getAnswerById(params.id);
  }, [getAnswerById, params.id]);
  return (
    <>
      <AnswerPage {...{ answer }} />
    </>
  );
};

PrintAnswer.propTypes = {
  getAnswerById: PropTypes.func,
  answer: PropTypes.object,
};

export default connect(
  state => ({
    answer: state.answers.answer,
  }),
  dispatch => ({
    getAnswerById: id => dispatch(getAnswerByIdAction(id)),
  }),
)(PrintAnswer);
