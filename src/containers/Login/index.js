import React from 'react';
import { useSelector } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import i18n from 'i18next';
import { ButtonGoogle } from 'components/common';

const Login = () => {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  if (isAuthenticated) {
    return <Redirect to="/" />;
  }
  return (
    <>
      <p className="loginHeader">{i18n.t('login.header')}</p>
      <ButtonGoogle />
    </>
  );
};

export default withRouter(Login);
