import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Icon, Dropdown, Menu } from 'antd';
import {
  deletePositionAction,
  getDefaultPositionsAction,
  getNextPositionsAction,
  getPreviousPositionsAction,
  unSubscribeAction,
} from '../../redux/positions/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import {
  lastPositionSelector,
  firstPositionSelector,
} from '../../redux/positions/selectors';
import CustomTable from '../../components/common/CustomTable';

const PosistionTable = ({
  positionsByPage,
  showModalWithItem,
  deletePosition,
  getDefaultPositions,
  getPreviousPositions,
  getNextPositions,
  lastPosition,
  firstPosition,
  disableNext,
  disablePrev,
  unSubscribe,
}) => {
  useEffect(() => {
    getDefaultPositions();
    return () => {
      unSubscribe();
    };
  }, [getDefaultPositions, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/positions/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    if (positionsByPage.length === 1) getPreviousPositions({ firstPosition });
    deletePosition({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
    <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstPosition}
        lastData={lastPosition}
        {...{
          disableNext,
          disablePrev,
          columns,
        }}
        getPreviousData={() => getPreviousPositions({ firstPosition })}
        getNextData={() => getNextPositions({ lastPosition })}
        dataSource={positionsByPage}
      />
    </>
  );
};

PosistionTable.propTypes = {
  positionsByPage: PropTypes.array,
  deletePosition: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getDefaultPositions: PropTypes.func,
  getPreviousPositions: PropTypes.func,
  getNextPositions: PropTypes.func,
  lastPosition: PropTypes.object,
  firstPosition: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  unSubscribe: PropTypes.func,
};

const mapStateToProps = state => ({
  positionsByPage: state.positions.positionsByPage,
  lastPosition: lastPositionSelector(state),
  firstPosition: firstPositionSelector(state),
  disableNext: state.pagingButtons.disableNext,
  disablePrev: state.pagingButtons.disablePrev,
});

const mapDispatchToProps = dispatch => ({
  deletePosition: params => dispatch(deletePositionAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getDefaultPositions: () => dispatch(getDefaultPositionsAction()),
  getNextPositions: params => dispatch(getNextPositionsAction(params)),
  getPreviousPositions: params => dispatch(getPreviousPositionsAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PosistionTable);
