import React, { useEffect, createRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useParams } from 'react-router';
import { getPreviewInterviewByIdAction } from 'redux/previewInterview/actions';
import { Header, Footer, FormInterview } from 'components/InterviewForm';

const InterviewForm = ({
  getPreviewInterviewById,
  previewInterview: { previewInterview },
}) => {
  const params = useParams();
  useEffect(() => {
    getPreviewInterviewById(params.id);
  }, [getPreviewInterviewById, params.id]);

  const arrRef = [];

  if (previewInterview) {
    previewInterview.questions.map((question, index) => {
      arrRef[index] = createRef(null);
      return null;
    });
  }
  return (
    previewInterview && (
      <>
        <Header />
        <FormInterview {...{ previewInterview, arrRef }} />
        <Footer />
      </>
    )
  );
};

InterviewForm.propTypes = {
  getPreviewInterviewById: PropTypes.func.isRequired,
  previewInterview: PropTypes.object.isRequired,
};

export default connect(
  state => ({
    previewInterview: state.previewInterview,
  }),
  dispatch => ({
    getPreviewInterviewById: interviewId =>
      dispatch(getPreviewInterviewByIdAction(interviewId)),
  }),
)(InterviewForm);
