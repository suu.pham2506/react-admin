import { connect } from 'react-redux';
import { uploadImageAction } from '../../../redux/firebaseImg/actions';
import { CustomUpload } from '../../../components/common';

const mapDispatchToProps = dispatch => ({
  uploadImage: (file, onSuccess) =>
    dispatch(uploadImageAction(file, onSuccess)),
});

export default connect(
  null,
  mapDispatchToProps,
)(CustomUpload);
