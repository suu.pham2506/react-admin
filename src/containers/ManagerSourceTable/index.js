import React, { useEffect } from 'react';
import { Icon, Dropdown, Menu } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'i18next';
import {
  getAllManagerSourceAction,
  deleteSourceAction,
  getNextSourcesAction,
  getPrevSourcesAction,
  unSubscribeAction,
} from '../../redux/managerSource/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import {
  lastSourceSelector,
  firstSourceSelector,
} from '../../redux/managerSource/selectors';
import CustomTable from '../../components/common/CustomTable';

const ManagerSourceTableContainer = ({
  managerSources: { managerSources },
  getAllManagerSource,
  deleteSource,
  showModalWithItem,
  firstSource,
  lastSource,
  disableNext,
  disablePrev,
  getNextSources,
  getPreviousSources,
  unSubscribe,
}) => {
  useEffect(() => {
    getAllManagerSource();
    return () => {
      unSubscribe();
    };
  }, [getAllManagerSource, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/manager-source/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDele = record => {
    deleteSource({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDele,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'data.name',
      key: 'data.name',
    },
    {
      title: 'Type',
      dataIndex: 'data.type',
      key: 'data.type',
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
    <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstSource}
        lastData={lastSource}
        {...{
          disableNext,
          disablePrev,
          columns,
        }}
        getPreviousData={() => getPreviousSources({ firstSource })}
        getNextData={() => getNextSources({ lastSource })}
        dataSource={managerSources}
        rowKey={record => record.id}
      />
    </>
  );
};

ManagerSourceTableContainer.propTypes = {
  managerSources: PropTypes.object,
  deleteSource: PropTypes.func,
  showModalWithItem: PropTypes.func,
  firstSource: PropTypes.object,
  lastSource: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  getNextSources: PropTypes.func,
  getPreviousSources: PropTypes.func,
  getAllManagerSource: PropTypes.func,
  unSubscribe: PropTypes.func,
};

export default connect(
  state => ({
    managerSources: state.managerSource,
    firstSource: firstSourceSelector(state),
    lastSource: lastSourceSelector(state),
    disableNext: state.pagingButtons.disableNext,
    disablePrev: state.pagingButtons.disablePrev,
  }),
  dispatch => ({
    getAllManagerSource: () => dispatch(getAllManagerSourceAction()),
    deleteSource: params => dispatch(deleteSourceAction(params)),
    showModalWithItem: params => dispatch(showModalWithItemAction(params)),
    getNextSources: params => dispatch(getNextSourcesAction(params)),
    getPreviousSources: params => dispatch(getPrevSourcesAction(params)),
    unSubscribe: () => dispatch(unSubscribeAction()),
  }),
)(ManagerSourceTableContainer);
