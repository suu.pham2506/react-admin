import { connect } from 'react-redux';
import { AnswerTable } from 'components/Table';
import {
  lastAnswerSelector,
  firstAnswerSelector,
} from 'redux/answers/selector';
import {
  deleteAnswerAction,
  getDefaultAnswersAction,
  getNextAnswersAction,
  getPreviousAnswersAction,
  unSubscribeAction,
} from 'redux/answers/actions';

const mapStateToProps = state => ({
  answersByPage: state.answers.answersByPage,
  lastAnswer: lastAnswerSelector(state),
  firstAnswer: firstAnswerSelector(state),
  disableNext: state.pagingButtons.disableNext,
  disablePrev: state.pagingButtons.disablePrev,
});

const mapDispatchToProps = dispatch => ({
  deleteAnswer: params => dispatch(deleteAnswerAction(params)),
  getDefaultAnswers: () => dispatch(getDefaultAnswersAction()),
  getNextAnswers: params => dispatch(getNextAnswersAction(params)),
  getPreviousAnswers: params => dispatch(getPreviousAnswersAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AnswerTable);
