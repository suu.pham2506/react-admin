import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Icon, Dropdown, Menu } from 'antd';
import { showModalWithItemAction } from 'redux/modal/actions';
import { getImageUrlAction } from 'redux/firebaseImg/actions';
import CustomTable from 'components/common/CustomTable';
import CustomAvatar from 'components/common/CustomAvatar';
import {
  deleteEmployeeAction,
  getDefaultEmployeesAction,
  getNextEmployeesAction,
  getPreviousEmployeesAction,
  unSubscribeAction,
} from 'redux/employees/actions';
import {
  lastEmployeeSelector,
  firstEmployeeSelector,
} from 'redux/employees/selectors';

const EmployeeContainer = ({
  employeesByPage,
  showModalWithItem,
  deleteEmployee,
  getDefaultEmployees,
  getPreviousEmployees,
  getNextEmployees,
  lastEmployee,
  firstEmployee,
  disableNext,
  disablePrev,
  unSubscribe,
  getImageUrl,
}) => {
  useEffect(() => {
    getDefaultEmployees();
    return () => {
      unSubscribe();
    };
  }, [getDefaultEmployees, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/employees/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    if (employeesByPage.length === 1) getPreviousEmployees({ firstEmployee });
    deleteEmployee({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name',
      render: (name, row) => (
        <span>
          <CustomAvatar imgName={row.imgName} getImgURL={getImageUrl} />
          <span className="email-field">{name}</span>
        </span>
      ),
    },
    {
      title: i18n.t('table.title.birthday'),
      dataIndex: 'birthday',
      render: birthday => (
        <span>{new Date(birthday).toLocaleDateString()}</span>
      ),
      key: 'birthday',
    },
    {
      title: i18n.t('table.title.job'),
      dataIndex: 'job',
      key: 'job',
    },
    {
      title: i18n.t('table.title.email'),
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: i18n.t('table.title.startWorkDate'),
      dataIndex: 'dayInCompany',
      render: startWorkDate => (
        <span>{new Date(startWorkDate).toLocaleDateString()}</span>
      ),
      key: 'dayInCompany',
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={(
<Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
)}
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstEmployee}
        lastData={lastEmployee}
        {...{
          disableNext,
          disablePrev,
          columns,
        }}
        getPreviousData={() => getPreviousEmployees({ firstEmployee })}
        getNextData={() => getNextEmployees({ lastEmployee })}
        dataSource={employeesByPage}
      />
    </>
  );
};

EmployeeContainer.propTypes = {
  employeesByPage: PropTypes.array,
  deleteEmployee: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getDefaultEmployees: PropTypes.func,
  getPreviousEmployees: PropTypes.func,
  getNextEmployees: PropTypes.func,
  lastEmployee: PropTypes.object,
  firstEmployee: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  unSubscribe: PropTypes.func,
  getImageUrl: PropTypes.func,
};

const mapStateToProps = state => ({
  employeesByPage: state.employees.employeesByPage,
  lastEmployee: lastEmployeeSelector(state),
  firstEmployee: firstEmployeeSelector(state),
  disableNext: state.pagingButtons.disableNext,
  disablePrev: state.pagingButtons.disablePrev,
});

const mapDispatchToProps = dispatch => ({
  deleteEmployee: params => dispatch(deleteEmployeeAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getDefaultEmployees: () => dispatch(getDefaultEmployeesAction()),
  getNextEmployees: params => dispatch(getNextEmployeesAction(params)),
  getPreviousEmployees: params => dispatch(getPreviousEmployeesAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
  getImageUrl: (imgName, setURL) =>
    dispatch(getImageUrlAction(imgName, setURL)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EmployeeContainer);
