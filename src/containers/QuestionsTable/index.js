import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Table, Button, Col, Row } from 'antd';
import { connect } from 'react-redux';
import { useParams, useRouteMatch } from 'react-router-dom';
import {
  getPreviewInterviewByIdAction,
  deleteQuestionAction,
  editQuestionAction,
} from '../../redux/previewInterview/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';

const QuestionsTable = ({
  previewInterview: { previewInterview },
  getPreviewInterviewById,
  deleteQuestion,
  editQuestion,
  showModalWithItem,
}) => {
  const [editingKey, setEditingKey] = useState('');
  const match = useRouteMatch();
  const params = useParams();
  const isEditing = row => row.id === editingKey;

  const handleEdit = key => {
    setEditingKey(key);
  };

  const handleCancel = () => {
    setEditingKey('');
  };

  useEffect(() => {
    getPreviewInterviewById(params.id);
  }, [getPreviewInterviewById, params.id]);

  const handleDelete = questionId => {
    deleteQuestion(params.id, questionId);
  };
  const typeInput = useRef();
  const textInput = useRef();
  const handleSave = questionId => {
    const data = {
      type: typeInput.current.value,
      text: textInput.current.value,
    };
    editQuestion(params.id, questionId, data);
    setEditingKey('');
  };

  const gotoEditPage = () => {
    const route = '/preview-interview/question/edit';
    showModalWithItem({ route, item: { match, previewInterview } });
  };

  const columns = [
    {
      title: 'Type',
      dataIndex: 'data.type',
      key: 'data.title',
      editable: true,
      render: (text, row) => {
        const editTable = isEditing(row);
        return editTable ? (
          <input defaultValue={text} ref={typeInput} />
        ) : (
          <p>{text}</p>
        );
      },
    },
    {
      title: 'Text',
      dataIndex: 'data.text',
      key: 'data.introduction',
      editable: true,
      render: (text, row) => {
        const editTable = isEditing(row);
        return editTable ? (
          <input defaultValue={text} ref={textInput} />
        ) : (
          <p>{text}</p>
        );
      },
    },
    {
      render: (text, row) => {
        const editTable = isEditing(row);
        return editTable ? (
          <>
            <Button type="primary" onClick={() => handleSave(row.id)}>
              Save
            </Button>

            <Button type="primary" onClick={handleCancel}>
              Cancel
            </Button>
          </>
        ) : (
          <>
            <Button
              disabled={editingKey !== ''}
              type="primary"
              onClick={() => handleEdit(row.id)}
            >
              Edit
            </Button>

            <Button
              disabled={editingKey !== ''}
              type="primary"
              onClick={() => handleDelete(row.id)}
            >
              Delete
            </Button>
          </>
        );
      },
    },
  ];
  return (
    previewInterview && (
      <>
        <Row type="flex" justify="space-between" align="middle">
          <Col>
            <h1>
              Title:
              {previewInterview.title}
            </h1>
            <h1>
              Introduce:
              {previewInterview.introduce}
            </h1>
          </Col>
          <Col>
            <Button type="primary" onClick={gotoEditPage}>
              Edit
            </Button>
          </Col>
        </Row>
        <Table
          rowKey={record => record.id}
          dataSource={previewInterview.questions}
          columns={columns}
          bordered
        />
      </>
    )
  );
};

QuestionsTable.propTypes = {
  previewInterview: PropTypes.object.isRequired,
  getPreviewInterviewById: PropTypes.func,
  showModalWithItem: PropTypes.func,
};

export default connect(
  state => ({
    previewInterview: state.previewInterview,
  }),
  dispatch => ({
    getPreviewInterviewById: id => dispatch(getPreviewInterviewByIdAction(id)),
    deleteQuestion: (previewInterviewId, questionId) =>
      dispatch(deleteQuestionAction(previewInterviewId, questionId)),
    editQuestion: (previewInterviewId, questionId, data) =>
      dispatch(editQuestionAction(previewInterviewId, questionId, data)),
    showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  }),
)(QuestionsTable);
