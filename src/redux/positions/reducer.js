import { makeReducerCreator } from '../../utils/reduxUtils';
import { PositionsTypes } from './actions';

export const initialState = {
  positionsByPage: [],
  positions: [],
  error: null,
};

const createPositionSuccess = state => ({
  ...state,
  error: null,
});

const createPositionFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deletePositionSuccess = (state, action) => ({
  ...state,
  error: null,
  positionsByPage: state.positionsByPage.filter(
    position => position.id !== action.id,
  ),
});

const deletePositionFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editPositionSuccess = (state, action) => ({
  ...state,
  error: null,
  positionsByPage: state.positionsByPage.map(position =>
    position.id === action.id
      ? { ...position, role: action.position.role }
      : position,
  ),
});

const editPositionFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getPositionsSuccess = (state, action) => ({
  ...state,
  positionsByPage: action.positionsByPage,
});

const getAllPositionsSuccess = (state, action) => ({
  ...state,
  positions: action.positions,
});

const getPositionsFail = (state, action) => ({
  ...state,
  error: action.error,
});

const positions = makeReducerCreator(initialState, {
  [PositionsTypes.CREATE_POSITION_FAIL]: createPositionFail,
  [PositionsTypes.CREATE_POSITION_SUCCESS]: createPositionSuccess,
  [PositionsTypes.DELETE_POSITION_FAIL]: deletePositionFail,
  [PositionsTypes.DELETE_POSITION_SUCCESS]: deletePositionSuccess,
  [PositionsTypes.EDIT_POSITION_FAIL]: editPositionFail,
  [PositionsTypes.EDIT_POSITION_SUCCESS]: editPositionSuccess,
  [PositionsTypes.GET_POSITIONS_FAIL]: getPositionsFail,
  [PositionsTypes.GET_POSITIONS_SUCCESS]: getPositionsSuccess,
  [PositionsTypes.GET_ALL_POSITIONS_SUCCESS]: getAllPositionsSuccess,
});

export default positions;
