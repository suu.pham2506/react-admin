import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const PositionsTypes = makeConstantCreator(
  'GET_DEFAULT_POSITIONS',
  'GET_POSITIONS_SUCCESS',
  'GET_POSITIONS_FAIL',
  'GET_NEXT_POSITIONS',
  'GET_PREVIOUS_POSITIONS',
  'CREATE_POSITION',
  'CREATE_POSITION_SUCCESS',
  'CREATE_POSITION_FAIL',
  'DELETE_POSITION',
  'DELETE_POSITION_SUCCESS',
  'DELETE_POSITION_FAIL',
  'EDIT_POSITION',
  'EDIT_POSITION_SUCCESS',
  'EDIT_POSITION_FAIL',
  'UNSUBSCRIBE',
  'GET_ALL_POSITIONS',
  'GET_ALL_POSITIONS_SUCCESS',
);

export const createPositionAction = ({ position }) =>
  makeActionCreator(PositionsTypes.CREATE_POSITION, { position });

export const createPositionSuccessAction = () =>
  makeActionCreator(PositionsTypes.CREATE_POSITION_SUCCESS);

export const createPositionFailAction = ({ error }) =>
  makeActionCreator(PositionsTypes.CREATE_POSITION_FAIL, { error });

export const deletePositionAction = ({ id }) =>
  makeActionCreator(PositionsTypes.DELETE_POSITION, { id });

export const deletePositionSuccessAction = ({ id }) =>
  makeActionCreator(PositionsTypes.DELETE_POSITION_SUCCESS, { id });

export const deletePositionFailAction = ({ error }) =>
  makeActionCreator(PositionsTypes.DELETE_POSITION_FAIL, { error });

export const editPositionAction = ({ id, position }) =>
  makeActionCreator(PositionsTypes.EDIT_POSITION, { id, position });

export const editPositionSuccessAction = ({ id, position }) =>
  makeActionCreator(PositionsTypes.EDIT_POSITION_SUCCESS, { id, position });

export const editPositionFailAction = ({ error }) =>
  makeActionCreator(PositionsTypes.EDIT_POSITION_FAIL, { error });

export const getDefaultPositionsAction = () =>
  makeActionCreator(PositionsTypes.GET_DEFAULT_POSITIONS);

export const getPositionsSuccessAction = ({ positionsByPage }) =>
  makeActionCreator(PositionsTypes.GET_POSITIONS_SUCCESS, { positionsByPage });

export const getAllPositionsAction = () =>
  makeActionCreator(PositionsTypes.GET_ALL_POSITIONS);

export const getAllPositionsSuccessAction = ({ positions }) =>
  makeActionCreator(PositionsTypes.GET_ALL_POSITIONS_SUCCESS, {
    positions,
  });

export const getPositionsFailAction = ({ error }) =>
  makeActionCreator(PositionsTypes.GET_POSITIONS_FAIL, { error });

export const getNextPositionsAction = ({ lastPosition }) =>
  makeActionCreator(PositionsTypes.GET_NEXT_POSITIONS, { lastPosition });

export const getPreviousPositionsAction = ({ firstPosition }) =>
  makeActionCreator(PositionsTypes.GET_PREVIOUS_POSITIONS, { firstPosition });

export const unSubscribeAction = () =>
  makeActionCreator(PositionsTypes.UNSUBSCRIBE);
