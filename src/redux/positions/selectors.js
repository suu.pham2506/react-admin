import { createSelector } from 'reselect';

const positionsByPageSelector = state => state.positions.positionsByPage;

export const lastPositionSelector = createSelector(
  positionsByPageSelector,
  positionsByPage => (positionsByPage[9] ? positionsByPage[9] : null),
);

export const firstPositionSelector = createSelector(
  positionsByPageSelector,
  positionsByPage => (positionsByPage[0] ? positionsByPage[0] : null),
);
