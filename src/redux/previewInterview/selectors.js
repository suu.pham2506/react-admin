import { createSelector } from 'reselect';

const previewInterviewsSelector = state =>
  state.previewInterview.previewInterviews;

export const lastInterviewSelector = createSelector(
  previewInterviewsSelector,
  previewInterviews => (previewInterviews[9] ? previewInterviews[9] : null),
);

export const firstInterviewSelector = createSelector(
  previewInterviewsSelector,
  previewInterviews => (previewInterviews[0] ? previewInterviews[0] : null),
);
