import { PreviewInterviewTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const initialState = {
  previewInterviews: [],
  previewInterview: {
    title: '',
    introduce: '',
    questions: [],
  },
};

export const getAllPreviewInterviewSuccess = (state, { data }) => ({
  ...state,
  previewInterviews: data,
});

export const getPreviewInterviewByIdSuccess = (state, { data }) => ({
  ...state,
  previewInterview: data,
});

export const addPreviewInterviewSuccess = (state, { data }) => ({
  ...state,
  previewInterviews: [data, ...state.previewInterviews],
});

export const deletePreviewInterviewSuccess = (state, { id }) => ({
  ...state,
  previewInterviews: state.previewInterviews.filter(
    previewInterview => previewInterview.id !== id,
  ),
});

export const addQuestionSuccessAction = (state, { data }) => ({
  ...state,
  previewInterview: {
    ...state.previewInterview,
    questions: [...state.previewInterview.questions, data],
  },
});

export const deleteQuestionSuccessAction = (state, { questionId }) => ({
  ...state,
  previewInterview: {
    ...state.previewInterview,
    questions: state.previewInterview.questions.filter(
      question => question.id !== questionId,
    ),
  },
});

export const editQuestionSuccessAction = (state, { questionId, data }) => ({
  ...state,
  previewInterview: {
    ...state.previewInterview,
    questions: state.previewInterview.questions.map(question => {
      if (question.id === questionId) {
        // eslint-disable-next-line no-param-reassign
        question.data = data;
      }
      return question;
    }),
  },
});
export const editPreviewInterviewSuccessAction = (state, { data }) => ({
  ...state,
  previewInterview: {
    ...state.previewInterview,
    title: data.data.title,
    introduce: data.data.introduce,
  },
});

export default makeReducerCreator(initialState, {
  [PreviewInterviewTypes.GET_All_PREVIEW_INTERVIEW_SUCCESS]: getAllPreviewInterviewSuccess,
  [PreviewInterviewTypes.GET_PREVIEW_INTERVIEW_BY_ID_SUCCESS]: getPreviewInterviewByIdSuccess,
  [PreviewInterviewTypes.ADD_PREVIEW_INTERVIEW_SUCCESS]: addPreviewInterviewSuccess,
  [PreviewInterviewTypes.DELETE_PREVIEW_INTERVIEW_SUCCESS]: deletePreviewInterviewSuccess,
  [PreviewInterviewTypes.ADD_QUESTION_SUCCESS]: addQuestionSuccessAction,
  [PreviewInterviewTypes.DELETE_QUESTION_SUCCESS]: deleteQuestionSuccessAction,
  [PreviewInterviewTypes.EDIT_QUESTION_SUCCESS]: editQuestionSuccessAction,
  [PreviewInterviewTypes.EDIT_PREVIEW_INTERVIEW_SUCCESS]: editPreviewInterviewSuccessAction,
});
