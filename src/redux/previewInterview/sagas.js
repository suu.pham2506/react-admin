import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import { push } from 'connected-react-router';
import {
  getAllPreviewInterviewSuccess,
  getPreviewInterviewByIdSuccsses,
  PreviewInterviewTypes,
  deletePreviewInterviewSuccess,
  deleteQuestionSuccessAction,
  addQuestionSuccessAction,
  editQuestionSuccessAction,
  editPreviewInterviewSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';
import { changeLoading } from '../loading/actions';

function* addQuestionSaga({ id, data }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('pre-interviews')
      .doc(id)
      .collection('questions')
      .add(data);
    yield put(addQuestionSuccessAction({ id: response.id, data }));
  } catch (error) {
    console.error(error);
  }
}

function* editPreviewInterviewSaga({ previewInterviewId, data }) {
  try {
    yield firebase
      .firestore()
      .collection('pre-interviews')
      .doc(previewInterviewId)
      .update(data);
    yield put(editPreviewInterviewSuccessAction({ data }));
  } catch (error) {
    console.error(error);
  }
}

function* editQuestionSaga({ previewInterviewId, questionId, data }) {
  try {
    yield firebase
      .firestore()
      .collection('pre-interviews')
      .doc(previewInterviewId)
      .collection('questions')
      .doc(questionId)
      .update(data);
    yield put(editQuestionSuccessAction(questionId, data));
  } catch (error) {
    console.error(error);
  }
}

function* deleteQuestionSaga({ previewInterviewId, questionId }) {
  try {
    yield firebase
      .firestore()
      .collection('pre-interviews')
      .doc(previewInterviewId)
      .collection('questions')
      .doc(questionId)
      .delete();
    yield put(deleteQuestionSuccessAction(questionId));
  } catch (error) {
    console.error(error);
  }
}

function* addPreviewInterviewSaga({ data }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('pre-interviews')
      .add(data);
    yield put(push(`/preview-interview/${response.id}`));
  } catch (error) {
    console.error(error);
  }
}

function* deletePreviewInterviewSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('pre-interviews')
      .doc(id)
      .delete();
    yield put(deletePreviewInterviewSuccess(id));
  } catch (error) {
    console.error(error);
  }
}

let channel = null;

function* getAllPreviewInterviewSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    const db = firebase.firestore();
    channel = eventChannel(emit => {
      const listener = db
        .collection('pre-interviews')
        .orderBy('title')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      let data = [];
      docs.forEach(doc => {
        data = [...data, { id: doc.id, data: doc.data() }];
      });
      yield put(getAllPreviewInterviewSuccess(data));
      yield put(defaultButtonsAction());
    }
  } catch (error) {
    console.error(error);
  }
}

function* getNextPreviewInterviewSaga({ lastInterview }) {
  try {
    const db = firebase.firestore();
    const response = yield db
      .collection('pre-interviews')
      .orderBy('title')
      .startAfter(lastInterview.data.title)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = db
          .collection('pre-interviews')
          .orderBy('title')
          .startAfter(lastInterview.data.title)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllPreviewInterviewSuccess(data));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (error) {
    console.error(error);
  }
}

function* getPrevPreviewInterviewSaga({ firstInterview }) {
  try {
    const db = firebase.firestore();
    const response = yield db
      .collection('pre-interviews')
      .orderBy('title', 'desc')
      .startAfter(firstInterview.data.title)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = db
          .collection('pre-interviews')
          .orderBy('title')
          .startAt(response.docs[9].data().data.title)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllPreviewInterviewSuccess(data));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (error) {
    console.error(error);
  }
}

function unSubscribeSaga() {
  if (channel !== null) {
    channel.close();
  }
}

function* getPreviewInterviewByIdSaga({ id }) {
  try {
    yield put(changeLoading(true));
    let questions = [];
    const db = firebase.firestore();
    const snapshot = yield db
      .collection('pre-interviews')
      .doc(id)
      .collection('questions')
      .get();
    snapshot.forEach(doc => {
      questions = [...questions, { id: doc.id, data: doc.data() }];
    });

    const titleData = yield db
      .collection('pre-interviews')
      .doc(id)
      .get();

    const { title, introduce } = titleData.data();

    if (questions && title && introduce) {
      const data = {
        introduce,
        title,
        questions,
      };
      yield put(changeLoading(false));
      yield put(getPreviewInterviewByIdSuccsses(data));
    }
  } catch (error) {
    yield put(changeLoading(false));
  }
}

export default [
  takeEvery(
    PreviewInterviewTypes.GET_ALL_PREVIEW_INTERVIEW,
    getAllPreviewInterviewSaga,
  ),
  takeEvery(
    PreviewInterviewTypes.GET_PREVIEW_INTERVIEW_BY_ID,
    getPreviewInterviewByIdSaga,
  ),
  takeEvery(
    PreviewInterviewTypes.ADD_PREVIEW_INTERVIEW,
    addPreviewInterviewSaga,
  ),
  takeEvery(
    PreviewInterviewTypes.DELETE_PREVIEW_INTERVIEW,
    deletePreviewInterviewSaga,
  ),
  takeEvery(PreviewInterviewTypes.ADD_QUESTION, addQuestionSaga),
  takeEvery(PreviewInterviewTypes.DELETE_QUESTION, deleteQuestionSaga),
  takeEvery(PreviewInterviewTypes.EDIT_QUESTION, editQuestionSaga),
  takeEvery(
    PreviewInterviewTypes.EDIT_PREVIEW_INTERVIEW,
    editPreviewInterviewSaga,
  ),
  takeEvery(
    PreviewInterviewTypes.GET_NEXT_PREVIEW_INTERVIEWS,
    getNextPreviewInterviewSaga,
  ),
  takeEvery(
    PreviewInterviewTypes.GET_PREV_PREVIEW_INTERVIEWS,
    getPrevPreviewInterviewSaga,
  ),
  takeEvery(PreviewInterviewTypes.UNSUBSCRIBE, unSubscribeSaga),
];
