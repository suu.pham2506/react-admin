import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const PreviewInterviewTypes = makeConstantCreator(
  'GET_ALL_PREVIEW_INTERVIEW',
  'GET_All_PREVIEW_INTERVIEW_SUCCESS',
  'GET_PREVIEW_INTERVIEW_BY_ID',
  'GET_PREVIEW_INTERVIEW_BY_ID_SUCCESS',
  'ADD_PREVIEW_INTERVIEW',
  'ADD_PREVIEW_INTERVIEW_SUCCESS',
  'DELETE_PREVIEW_INTERVIEW',
  'DELETE_PREVIEW_INTERVIEW_SUCCESS',
  'ADD_QUESTION',
  'ADD_QUESTION_SUCCESS',
  'DELETE_QUESTION',
  'DELETE_QUESTION_SUCCESS',
  'EDIT_QUESTION',
  'EDIT_QUESTION_SUCCESS',
  'EDIT_PREVIEW_INTERVIEW',
  'EDIT_PREVIEW_INTERVIEW_SUCCESS',
  'GET_NEXT_PREVIEW_INTERVIEWS',
  'GET_PREV_PREVIEW_INTERVIEWS',
  'UNSUBSCRIBE',
);

export const getAllPreviewInterviewAction = () =>
  makeActionCreator(PreviewInterviewTypes.GET_ALL_PREVIEW_INTERVIEW);

export const getPreviewInterviewByIdAction = id =>
  makeActionCreator(PreviewInterviewTypes.GET_PREVIEW_INTERVIEW_BY_ID, { id });

export const getAllPreviewInterviewSuccess = data =>
  makeActionCreator(PreviewInterviewTypes.GET_All_PREVIEW_INTERVIEW_SUCCESS, {
    data,
  });

export const getPreviewInterviewByIdSuccsses = data =>
  makeActionCreator(PreviewInterviewTypes.GET_PREVIEW_INTERVIEW_BY_ID_SUCCESS, {
    data,
  });

export const addPreviewInterviewAction = data =>
  makeActionCreator(PreviewInterviewTypes.ADD_PREVIEW_INTERVIEW, {
    data,
  });

export const addPreviewInterviewSuccess = data =>
  makeActionCreator(PreviewInterviewTypes.ADD_PREVIEW_INTERVIEW_SUCCESS, {
    data,
  });

export const deletePreviewInterviewAction = id =>
  makeActionCreator(PreviewInterviewTypes.DELETE_PREVIEW_INTERVIEW, {
    id,
  });

export const deletePreviewInterviewSuccess = id =>
  makeActionCreator(PreviewInterviewTypes.DELETE_PREVIEW_INTERVIEW_SUCCESS, {
    id,
  });

export const addQuestionAction = (id, data) =>
  makeActionCreator(PreviewInterviewTypes.ADD_QUESTION, {
    id,
    data,
  });

export const addQuestionSuccessAction = data =>
  makeActionCreator(PreviewInterviewTypes.ADD_QUESTION_SUCCESS, {
    data,
  });

export const deleteQuestionAction = (previewInterviewId, questionId) =>
  makeActionCreator(PreviewInterviewTypes.DELETE_QUESTION, {
    previewInterviewId,
    questionId,
  });

export const deleteQuestionSuccessAction = questionId =>
  makeActionCreator(PreviewInterviewTypes.DELETE_QUESTION_SUCCESS, {
    questionId,
  });

export const editQuestionAction = (previewInterviewId, questionId, data) =>
  makeActionCreator(PreviewInterviewTypes.EDIT_QUESTION, {
    previewInterviewId,
    questionId,
    data,
  });

export const editQuestionSuccessAction = (questionId, data) =>
  makeActionCreator(PreviewInterviewTypes.EDIT_QUESTION_SUCCESS, {
    questionId,
    data,
  });

export const editPreviewInterviewAction = (previewInterviewId, data) =>
  makeActionCreator(PreviewInterviewTypes.EDIT_PREVIEW_INTERVIEW, {
    previewInterviewId,
    data,
  });

export const editPreviewInterviewSuccessAction = data =>
  makeActionCreator(PreviewInterviewTypes.EDIT_PREVIEW_INTERVIEW_SUCCESS, {
    data,
  });

export const getNextPreviewInterviewsAction = ({ lastInterview }) =>
  makeActionCreator(PreviewInterviewTypes.GET_NEXT_PREVIEW_INTERVIEWS, {
    lastInterview,
  });

export const getPrevPreviewInterviewsAction = ({ firstInterview }) =>
  makeActionCreator(PreviewInterviewTypes.GET_PREV_PREVIEW_INTERVIEWS, {
    firstInterview,
  });

export const unSubscribeAction = () =>
  makeActionCreator(PreviewInterviewTypes.UNSUBSCRIBE);
