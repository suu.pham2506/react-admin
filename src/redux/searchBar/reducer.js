import { makeReducerCreator } from '../../utils/reduxUtils';
import { SearchBarTypes } from './actions';

export const initialState = {
  results: [],
};

const changeSearchResults = (state, action) => ({
  ...state,
  results: action.results,
});

const searchBar = makeReducerCreator(initialState, {
  [SearchBarTypes.CHANGE_SEARCH_RESULTS]: changeSearchResults,
});

export default searchBar;
