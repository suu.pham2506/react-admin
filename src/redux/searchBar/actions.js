import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const SearchBarTypes = makeConstantCreator(
  'CHANGE_SEARCH_RESULTS',
  'ALGOLIA_SEARCH',
);

export const changeSearchResultsAction = results =>
  makeActionCreator(SearchBarTypes.CHANGE_SEARCH_RESULTS, { results });

export const algoliaSearchAction = ({ index, query }) =>
  makeActionCreator(SearchBarTypes.ALGOLIA_SEARCH, { index, query });
