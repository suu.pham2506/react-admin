import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const UsersTypes = makeConstantCreator(
  'GET_DEFAULT_USERS',
  'GET_USERS_SUCCESS',
  'GET_USERS_FAIL',
  'GET_NEXT_USERS',
  'GET_PREVIOUS_USERS',
  'CREATE_USER',
  'CREATE_USER_SUCCESS',
  'CREATE_USER_FAIL',
  'DELETE_USER',
  'DELETE_USER_SUCCESS',
  'DELETE_USER_FAIL',
  'EDIT_USER',
  'EDIT_USER_SUCCESS',
  'EDIT_USER_FAIL',
  'UNSUBSCRIBE',
  'GET_FILTERED_USERS',
);

export const createUserAction = ({ user }) =>
  makeActionCreator(UsersTypes.CREATE_USER, { user });

export const createUserSuccessAction = () =>
  makeActionCreator(UsersTypes.CREATE_USER_SUCCESS);

export const createUserFailAction = ({ error }) =>
  makeActionCreator(UsersTypes.CREATE_USER_FAIL, { error });

export const deleteUserAction = ({ id }) =>
  makeActionCreator(UsersTypes.DELETE_USER, { id });

export const deleteUserSuccessAction = ({ id }) =>
  makeActionCreator(UsersTypes.DELETE_USER_SUCCESS, { id });

export const deleteUserFailAction = ({ error }) =>
  makeActionCreator(UsersTypes.DELETE_USER_FAIL, { error });

export const editUserAction = ({ id, user }) =>
  makeActionCreator(UsersTypes.EDIT_USER, { id, user });

export const editUserSuccessAction = ({ id, user }) =>
  makeActionCreator(UsersTypes.EDIT_USER_SUCCESS, { id, user });

export const editUserFailAction = ({ error }) =>
  makeActionCreator(UsersTypes.EDIT_USER_FAIL, { error });

export const getDefaultUsersAction = () =>
  makeActionCreator(UsersTypes.GET_DEFAULT_USERS);

export const getUsersSuccessAction = ({ usersByPage }) =>
  makeActionCreator(UsersTypes.GET_USERS_SUCCESS, { usersByPage });

export const getUsersFailAction = ({ error }) =>
  makeActionCreator(UsersTypes.GET_USERS_FAIL, { error });

export const getNextUsersAction = ({ lastUser }) =>
  makeActionCreator(UsersTypes.GET_NEXT_USERS, { lastUser });

export const getPreviousUsersAction = ({ firstUser }) =>
  makeActionCreator(UsersTypes.GET_PREVIOUS_USERS, { firstUser });

export const unSubscribeAction = () =>
  makeActionCreator(UsersTypes.UNSUBSCRIBE);

export const getFilteredUsersAction = ({ docID }) =>
  makeActionCreator(UsersTypes.GET_FILTERED_USERS, { docID });
