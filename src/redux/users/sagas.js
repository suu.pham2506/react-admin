import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  UsersTypes,
  createUserFailAction,
  deleteUserFailAction,
  editUserFailAction,
  getUsersFailAction,
  getUsersSuccessAction,
  deleteUserSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

function* createUserSaga({ user }) {
  try {
    yield firebase
      .firestore()
      .collection('iam')
      .add(user);
  } catch (e) {
    yield put(createUserFailAction(e));
  }
}

function* editUserSaga({ id, user }) {
  try {
    yield firebase
      .firestore()
      .collection('iam')
      .doc(id)
      .update(user);
  } catch (e) {
    yield put(editUserFailAction(e));
  }
}

function* deleteUserSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('iam')
      .doc(id)
      .delete();
    yield put(deleteUserSuccessAction({ id }));
  } catch (e) {
    yield put(deleteUserFailAction(e));
  }
}

let channel = null;

function* getDefaultUsersSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('iam')
        .orderBy('email')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getUsersSuccessAction({ usersByPage: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getUsersFailAction(e));
  }
}

function* getFilteredUsersSaga({ docID }) {
  try {
    channel.close();
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('iam')
        .doc(docID)
        .onSnapshot(doc => {
          emit(doc);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const doc = yield take(channel);
      const responseData = [];
      responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      yield put(getUsersSuccessAction({ usersByPage: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    console.error(e.message);
  }
}

function* getNextUsersSaga({ lastUser }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('iam')
      .orderBy('email')
      .startAfter(lastUser.email)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('iam')
          .orderBy('email')
          .startAfter(lastUser.email)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getUsersSuccessAction({ usersByPage: responseData }));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getUsersFailAction(e));
  }
}

function* getPreviousUsersSaga({ firstUser }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('iam')
      .orderBy('email', 'desc')
      .startAfter(firstUser.email)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('iam')
          .orderBy('email')
          .startAt(response.docs[9].data().email)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getUsersSuccessAction({ usersByPage: responseData }));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getUsersFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(UsersTypes.CREATE_USER, createUserSaga),
  takeEvery(UsersTypes.DELETE_USER, deleteUserSaga),
  takeEvery(UsersTypes.EDIT_USER, editUserSaga),
  takeEvery(UsersTypes.GET_DEFAULT_USERS, getDefaultUsersSaga),
  takeEvery(UsersTypes.GET_NEXT_USERS, getNextUsersSaga),
  takeEvery(UsersTypes.GET_PREVIOUS_USERS, getPreviousUsersSaga),
  takeEvery(UsersTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(UsersTypes.GET_FILTERED_USERS, getFilteredUsersSaga),
];
