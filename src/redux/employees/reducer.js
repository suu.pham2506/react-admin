import { makeReducerCreator } from '../../utils/reduxUtils';
import { EmployeesTypes } from './actions';

export const initialState = {
  employeesByPage: [],
  error: null,
};

const createEmployeeSuccess = state => ({
  ...state,
  error: null,
});

const createEmployeeFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deleteEmployeeSuccess = (state, action) => ({
  ...state,
  error: null,
  employeesByPage: state.employeesByPage.filter(
    employee => employee.id !== action.id,
  ),
});

const deleteEmployeeFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editEmployeeSuccess = (state, action) => ({
  ...state,
  error: null,
  employeesByPage: state.employeesByPage.map(employee =>
    employee.id === action.id
      ? { ...employee, role: action.employee.role }
      : employee,
  ),
});

const editEmployeeFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getEmployeesSuccess = (state, action) => ({
  ...state,
  employeesByPage: action.employeesByPage,
});

const getEmployeesFail = (state, action) => ({
  ...state,
  error: action.error,
});

const employees = makeReducerCreator(initialState, {
  [EmployeesTypes.CREATE_EMPLOYEE_FAIL]: createEmployeeFail,
  [EmployeesTypes.CREATE_EMPLOYEE_SUCCESS]: createEmployeeSuccess,
  [EmployeesTypes.DELETE_EMPLOYEE_FAIL]: deleteEmployeeFail,
  [EmployeesTypes.DELETE_EMPLOYEE_SUCCESS]: deleteEmployeeSuccess,
  [EmployeesTypes.EDIT_EMPLOYEE_FAIL]: editEmployeeFail,
  [EmployeesTypes.EDIT_EMPLOYEE_SUCCESS]: editEmployeeSuccess,
  [EmployeesTypes.GET_EMPLOYEES_FAIL]: getEmployeesFail,
  [EmployeesTypes.GET_EMPLOYEES_SUCCESS]: getEmployeesSuccess,
});

export default employees;
