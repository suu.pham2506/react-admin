import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  EmployeesTypes,
  createEmployeeFailAction,
  deleteEmployeeFailAction,
  editEmployeeFailAction,
  getEmployeesFailAction,
  getEmployeesSuccessAction,
  deleteEmployeeSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

function* createEmployeeSaga({ employee }) {
  try {
    yield firebase
      .firestore()
      .collection('employees')
      .add(employee);
  } catch (e) {
    yield put(createEmployeeFailAction(e));
  }
}

function* editEmployeeSaga({ id, employee }) {
  try {
    yield firebase
      .firestore()
      .collection('employees')
      .doc(id)
      .update(employee);
  } catch (e) {
    yield put(editEmployeeFailAction(e));
  }
}

function* deleteEmployeeSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('employees')
      .doc(id)
      .delete();
    yield put(deleteEmployeeSuccessAction({ id }));
  } catch (e) {
    yield put(deleteEmployeeFailAction(e));
  }
}

let channel = null;

function* getDefaultEmployeesSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('employees')
        .orderBy('name')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getEmployeesSuccessAction({ employeesByPage: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getEmployeesFailAction(e));
  }
}

function* getNextEmployeesSaga({ lastEmployee }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('employees')
      .orderBy('name')
      .startAfter(lastEmployee.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('employees')
          .orderBy('name')
          .startAfter(lastEmployee.name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getEmployeesSuccessAction({ employeesByPage: responseData }));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getEmployeesFailAction(e));
  }
}

function* getPreviousEmployeesSaga({ firstEmployee }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('employees')
      .orderBy('name', 'desc')
      .startAfter(firstEmployee.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('employees')
          .orderBy('name')
          .startAt(response.docs[9].data().name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getEmployeesSuccessAction({ employeesByPage: responseData }));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getEmployeesFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(EmployeesTypes.CREATE_EMPLOYEE, createEmployeeSaga),
  takeEvery(EmployeesTypes.DELETE_EMPLOYEE, deleteEmployeeSaga),
  takeEvery(EmployeesTypes.EDIT_EMPLOYEE, editEmployeeSaga),
  takeEvery(EmployeesTypes.GET_DEFAULT_EMPLOYEES, getDefaultEmployeesSaga),
  takeEvery(EmployeesTypes.GET_NEXT_EMPLOYEES, getNextEmployeesSaga),
  takeEvery(EmployeesTypes.GET_PREVIOUS_EMPLOYEES, getPreviousEmployeesSaga),
  takeEvery(EmployeesTypes.UNSUBSCRIBE, unSubscribeSaga),
];
