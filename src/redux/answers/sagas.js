import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  AnswersTypes,
  createAnswerFailAction,
  deleteAnswerFailAction,
  editAnswerFailAction,
  getAnswersFailAction,
  getAnswersSuccessAction,
  deleteAnswerSuccessAction,
  getAnswerByIdFailAction,
  getAnswerByIdSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

function* createAnswerSaga({ answer }) {
  try {
    yield firebase
      .firestore()
      .collection('answers')
      .add(answer);
  } catch (e) {
    yield put(createAnswerFailAction(e));
  }
}

function* editAnswerSaga({ id, answer }) {
  try {
    yield firebase
      .firestore()
      .collection('answers')
      .doc(id)
      .update(answer);
  } catch (e) {
    yield put(editAnswerFailAction(e));
  }
}

function* deleteAnswerSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('answers')
      .doc(id)
      .delete();
    yield put(deleteAnswerSuccessAction({ id }));
  } catch (e) {
    yield put(deleteAnswerFailAction(e));
  }
}

let channel = null;

function* getDefaultAnswersSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('answers')
        .orderBy('created')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({
          ...doc.data(),
          id: doc.id,
          key: doc.id,
          name: JSON.parse(doc.data().part1).form_response.answers[0].text,
        });
      });
      yield put(getAnswersSuccessAction({ answersByPage: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getAnswersFailAction(e));
  }
}

function* getNextAnswersSaga({ lastAnswer }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('answers')
      .orderBy('created')
      .startAfter(lastAnswer.created)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('answers')
          .orderBy('created')
          .startAfter(lastAnswer.created)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getAnswersSuccessAction({ answersByPage: responseData }));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getAnswersFailAction(e));
  }
}

function* getPreviousAnswersSaga({ firstAnswer }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('answers')
      .orderBy('created', 'desc')
      .startAfter(firstAnswer.created)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('answers')
          .orderBy('created')
          .startAt(response.docs[9].data().created)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getAnswersSuccessAction({ answersByPage: responseData }));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getAnswersFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

function* getAnswerByIdSaga({ id }) {
  try {
    const snapshot = yield firebase
      .firestore()
      .collection('answers')
      .doc(id)
      .get();
    const answer = {
      id: snapshot.id,
      questions: [
        ...JSON.parse(snapshot.data().part1).form_response.definition.fields,
        ...JSON.parse(snapshot.data().part2).form_response.definition.fields,
      ],
      answers: [
        ...JSON.parse(snapshot.data().part1).form_response.answers,
        ...JSON.parse(snapshot.data().part2).form_response.answers,
      ],
      name: JSON.parse(snapshot.data().part1).form_response.answers[0].text,
    };
    yield put(getAnswerByIdSuccessAction({ answer }));
  } catch (error) {
    yield put(getAnswerByIdFailAction(error));
  }
}

export default [
  takeEvery(AnswersTypes.CREATE_ANSWER, createAnswerSaga),
  takeEvery(AnswersTypes.DELETE_ANSWER, deleteAnswerSaga),
  takeEvery(AnswersTypes.EDIT_ANSWER, editAnswerSaga),
  takeEvery(AnswersTypes.GET_DEFAULT_ANSWERS, getDefaultAnswersSaga),
  takeEvery(AnswersTypes.GET_NEXT_ANSWERS, getNextAnswersSaga),
  takeEvery(AnswersTypes.GET_PREVIOUS_ANSWERS, getPreviousAnswersSaga),
  takeEvery(AnswersTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(AnswersTypes.GET_ANSWER_BY_ID, getAnswerByIdSaga),
];
