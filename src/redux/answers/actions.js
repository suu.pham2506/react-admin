import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const AnswersTypes = makeConstantCreator(
  'GET_DEFAULT_ANSWERS',
  'GET_ANSWERS_SUCCESS',
  'GET_ANSWERS_FAIL',
  'GET_ANSWER_BY_ID',
  'GET_ANSWER_BY_ID_SUCCESS',
  'GET_ANSWER_BY_ID_FAIL',
  'GET_NEXT_ANSWERS',
  'GET_PREVIOUS_ANSWERS',
  'CREATE_ANSWER',
  'CREATE_ANSWER_SUCCESS',
  'CREATE_ANSWER_FAIL',
  'DELETE_ANSWER',
  'DELETE_ANSWER_SUCCESS',
  'DELETE_ANSWER_FAIL',
  'EDIT_ANSWER',
  'EDIT_ANSWER_SUCCESS',
  'EDIT_ANSWER_FAIL',
  'UNSUBSCRIBE',
);

export const createAnswerAction = ({ answer }) =>
  makeActionCreator(AnswersTypes.CREATE_ANSWER, { answer });

export const createAnswerSuccessAction = () =>
  makeActionCreator(AnswersTypes.CREATE_ANSWER_SUCCESS);

export const createAnswerFailAction = ({ error }) =>
  makeActionCreator(AnswersTypes.CREATE_ANSWER_FAIL, { error });

export const deleteAnswerAction = ({ id }) =>
  makeActionCreator(AnswersTypes.DELETE_ANSWER, { id });

export const deleteAnswerSuccessAction = ({ id }) =>
  makeActionCreator(AnswersTypes.DELETE_ANSWER_SUCCESS, { id });

export const deleteAnswerFailAction = ({ error }) =>
  makeActionCreator(AnswersTypes.DELETE_ANSWER_FAIL, { error });

export const editAnswerAction = ({ id, answer }) =>
  makeActionCreator(AnswersTypes.EDIT_ANSWER, { id, answer });

export const editAnswerSuccessAction = ({ id, answer }) =>
  makeActionCreator(AnswersTypes.EDIT_ANSWER_SUCCESS, { id, answer });

export const editAnswerFailAction = ({ error }) =>
  makeActionCreator(AnswersTypes.EDIT_ANSWER_FAIL, { error });

export const getDefaultAnswersAction = () =>
  makeActionCreator(AnswersTypes.GET_DEFAULT_ANSWERS);

export const getAnswersSuccessAction = ({ answersByPage }) =>
  makeActionCreator(AnswersTypes.GET_ANSWERS_SUCCESS, { answersByPage });

export const getAnswersFailAction = ({ error }) =>
  makeActionCreator(AnswersTypes.GET_ANSWERS_FAIL, { error });

export const getAnswerByIdAction = id =>
  makeActionCreator(AnswersTypes.GET_ANSWER_BY_ID, { id });

export const getAnswerByIdSuccessAction = ({ answer }) =>
  makeActionCreator(AnswersTypes.GET_ANSWER_BY_ID_SUCCESS, { answer });

export const getAnswerByIdFailAction = ({ error }) =>
  makeActionCreator(AnswersTypes.GET_ANSWER_BY_ID_FAIL, { error });

export const getNextAnswersAction = ({ lastAnswer }) =>
  makeActionCreator(AnswersTypes.GET_NEXT_ANSWERS, { lastAnswer });

export const getPreviousAnswersAction = ({ firstAnswer }) =>
  makeActionCreator(AnswersTypes.GET_PREVIOUS_ANSWERS, { firstAnswer });

export const unSubscribeAction = () =>
  makeActionCreator(AnswersTypes.UNSUBSCRIBE);
