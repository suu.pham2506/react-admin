import { makeReducerCreator } from '../../utils/reduxUtils';
import { AnswersTypes } from './actions';

export const initialState = {
  answersByPage: [],
  answer: {},
  error: null,
};

const createAnswerSuccess = state => ({
  ...state,
  error: null,
});

const createAnswerFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deleteAnswerSuccess = (state, action) => ({
  ...state,
  error: null,
  answersByPage: state.answersByPage.filter(answer => answer.id !== action.id),
});

const deleteAnswerFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editAnswerSuccess = (state, action) => ({
  ...state,
  error: null,
  answersByPage: state.answersByPage.map(answer =>
    answer.id === action.id ? { ...answer, role: action.answer.role } : answer,
  ),
});

const editAnswerFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getAnswersSuccess = (state, action) => ({
  ...state,
  answersByPage: action.answersByPage,
});

const getAnswersFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getAnswerByIdSuccess = (state, action) => ({
  ...state,
  answer: action.answer,
});

const getAnswerByIdFail = (state, action) => ({
  ...state,
  error: action.error,
});

const answers = makeReducerCreator(initialState, {
  [AnswersTypes.CREATE_ANSWER_FAIL]: createAnswerFail,
  [AnswersTypes.CREATE_ANSWER_SUCCESS]: createAnswerSuccess,
  [AnswersTypes.DELETE_ANSWER_FAIL]: deleteAnswerFail,
  [AnswersTypes.DELETE_ANSWER_SUCCESS]: deleteAnswerSuccess,
  [AnswersTypes.EDIT_ANSWER_FAIL]: editAnswerFail,
  [AnswersTypes.EDIT_ANSWER_SUCCESS]: editAnswerSuccess,
  [AnswersTypes.GET_ANSWERS_FAIL]: getAnswersFail,
  [AnswersTypes.GET_ANSWERS_SUCCESS]: getAnswersSuccess,
  [AnswersTypes.GET_ANSWER_BY_ID_FAIL]: getAnswerByIdFail,
  [AnswersTypes.GET_ANSWER_BY_ID_SUCCESS]: getAnswerByIdSuccess,
});

export default answers;
