import { createSelector } from 'reselect';

const answersByPageSelector = state => state.answers.answersByPage;

export const lastAnswerSelector = createSelector(
  answersByPageSelector,
  answersByPage => (answersByPage[9] ? answersByPage[9] : null),
);

export const firstAnswerSelector = createSelector(
  answersByPageSelector,
  answersByPage => (answersByPage[0] ? answersByPage[0] : null),
);
