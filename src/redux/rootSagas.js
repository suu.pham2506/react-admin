import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import usersSaga from './users/sagas';
import previewInterview from './previewInterview/sagas';
import managerSource from './managerSource/sagas';
import contact from './contact/sagas';
import searchBar from './searchBar/sagas';
import appointments from './appointments/sagas';
import positionsSaga from './positions/sagas';
import employeesSaga from './employees/sagas';
import firebaseImg from './firebaseImg/sagas';
import answersSaga from './answers/sagas';

export default function* root() {
  yield all([
    ...authSaga,
    ...previewInterview,
    ...managerSource,
    ...usersSaga,
    ...contact,
    ...searchBar,
    ...appointments,
    ...positionsSaga,
    ...employeesSaga,
    ...firebaseImg,
    ...answersSaga,
  ]);
}
