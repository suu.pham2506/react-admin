import { takeEvery, put, call, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import { onAuthStateChanged } from 'configs/firebase';
import {
  login,
  logout,
  logoutSuccess,
  getCurrentUser,
  loginSuccess,
  loginFailure,
  getCurentUserSuccess,
  getCurentUserFailure,
} from './reducer';

const provider = new firebase.auth.GoogleAuthProvider();

function* loginFirebaseSaga() {
  try {
    const result = yield firebase.auth().signInWithPopup(provider);
    const sessionToken = result.credential.accessToken;
    let idToken = yield result.user.getIdTokenResult(true);
    if (idToken.claims.admin) {
      localStorage.setItem('sessionToken', sessionToken);
      yield put(loginSuccess(result.user));
      yield put(
        getCurentUserSuccess({
          user: result.user,
          admin: idToken.claims.admin,
        }),
      );
    } else {
      const channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('metadata')
          .onSnapshot(snapshot => {
            snapshot.docChanges().forEach(change => {
              if (change.type === 'added') {
                if (change.doc.id === result.user.uid) {
                  emit(true);
                }
              }
            });
            setTimeout(() => emit(false), 3000);
          });
        return () => {
          listener();
        };
      });
      const isDocChanged = yield take(channel);
      channel.close();
      if (isDocChanged) {
        idToken = yield result.user.getIdTokenResult(true);
        if (idToken.claims.admin) {
          localStorage.setItem('sessionToken', sessionToken);
          yield put(loginSuccess(result.user));
          yield put(getCurentUserSuccess(result.user, idToken.claims.admin));
        } else {
          yield put(logoutSuccess());
        }
      } else {
        yield put(logoutSuccess());
      }
    }
  } catch (e) {
    console.error(e.message);
    yield put(loginFailure(e));
  }
}

function* logoutFirebaseSaga() {
  try {
    yield firebase.auth().signOut();
    yield put(logoutSuccess());
  } catch (e) {
    console.error(e.message);
  }
}

function* getCurrentUserFirebaseSaga() {
  try {
    const user = yield call(onAuthStateChanged);
    const idToken = yield user.getIdTokenResult(true);
    yield put(getCurentUserSuccess({ user, admin: idToken.claims.admin }));
  } catch (e) {
    yield put(getCurentUserFailure(e));
  }
}

export default [
  takeEvery(`${login}`, loginFirebaseSaga),
  takeEvery(`${logout}`, logoutFirebaseSaga),
  takeEvery(`${getCurrentUser}`, getCurrentUserFirebaseSaga),
];
