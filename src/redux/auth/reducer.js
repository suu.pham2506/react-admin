import { createSlice } from 'redux-starter-kit';

const initialState = {
  isAuthenticated: !!localStorage.getItem('sessionToken'),
  data: {},
  roles: false,
  loginError: false,
  loginSuccess: false,
  loading: false,
};

const authSlice = createSlice({
  name: 'Auth',
  initialState,
  reducers: {
    login: state => state,
    loginSuccess: (state, { payload }) => {
      state.isAuthenticated = true;
      state.loginSuccess = true;
      state.data = payload;
    },
    loginFailureAction: (state, { payload }) => {
      state.error = payload;
    },
    getCurrentUser: state => state,
    logout: state => state,
    logoutSuccess: state => {
      localStorage.removeItem('sessionToken');
      state.isAuthenticated = false;
      state.data = {};
      state.loading = false;
    },
    getCurentUserSuccess: (state, { payload }) => {
      state.data = payload.user;
      state.roles = payload.admin;
    },
    getCurentUserFailure: (state, { error }) => {
      state.error = error;
    },
  },
});

const { actions, reducer } = authSlice;

export const {
  login,
  logout,
  loginSuccess,
  loginFailure,
  getCurrentUser,
  getCurentUserSuccess,
  getCurentUserFailure,
  logoutSuccess,
} = actions;

export default reducer;
