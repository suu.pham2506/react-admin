import { takeEvery, put, take } from 'redux-saga/effects';
import firebase from 'firebase';
import { eventChannel } from 'redux-saga';
import {
  getAllContactSuccessAction,
  getAllContactFailureAction,
  ContactTypes,
  addContactFailureAction,
  addContactSuccessAction,
  deleteContactFailureAction,
  editContactFailureAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

let channel = null;

function* getAllContactSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('contacts')
        .orderBy('email')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      let data = [];
      docs.forEach(doc => {
        data = [...data, { id: doc.id, data: doc.data() }];
      });
      yield put(getAllContactSuccessAction(data));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getAllContactFailureAction());
  }
}

function* getNextContactsSaga({ lastContact }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('contacts')
      .orderBy('email')
      .startAfter(lastContact.data.email)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('contacts')
          .orderBy('email')
          .startAfter(lastContact.data.email)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllContactSuccessAction(data));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getAllContactFailureAction());
  }
}

function* getPrevContactsSaga({ firstContact }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('contacts')
      .orderBy('email', 'desc')
      .startAfter(firstContact.data.email)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('contacts')
          .orderBy('email')
          .startAt(response.docs[9].data().email)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllContactSuccessAction(data));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getAllContactFailureAction());
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

function* editContactSaga({ id, contact }) {
  try {
    yield firebase
      .firestore()
      .collection('contacts')
      .doc(id)
      .update(contact);
  } catch (e) {
    yield put(editContactFailureAction(e));
  }
}

function* addContactSaga({ contact }) {
  try {
    const responese = yield firebase
      .firestore()
      .collection('contacts')
      .add(contact);
    if (responese.id) {
      yield put(addContactSuccessAction());
    }
  } catch (e) {
    yield put(addContactFailureAction());
  }
}

function* deleteContactSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('contacts')
      .doc(id)
      .delete();
  } catch (e) {
    yield put(deleteContactFailureAction(e));
  }
}

export default [
  takeEvery(ContactTypes.GET_ALL_CONTACT, getAllContactSaga),
  takeEvery(ContactTypes.ADD_CONTACT, addContactSaga),
  takeEvery(ContactTypes.DELETE_CONTACT, deleteContactSaga),
  takeEvery(ContactTypes.GET_NEXT_CONTACTS, getNextContactsSaga),
  takeEvery(ContactTypes.GET_PREV_CONTACTS, getPrevContactsSaga),
  takeEvery(ContactTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(ContactTypes.EDIT_CONTACT, editContactSaga),
];
