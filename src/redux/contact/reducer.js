import { ContactTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const initialState = {
  contacts: [],
  error: null,
};

export const getAllContactSuccess = (state, { data }) => ({
  ...state,
  contacts: data,
});

export const getAllContactFailure = (state, action) => ({
  ...state,
  error: action.error,
});

export const addContactSuccess = state => ({
  ...state,
  error: null,
});
export const addContactFailure = (state, action) => ({
  ...state,
  error: action.error,
});

export const deleteContactSuccess = state => ({
  ...state,
  error: null,
});
export const deleteContactFailure = (state, action) => ({
  ...state,
  error: action.error,
});

export const editContactSuccess = (state, action) => ({
  ...state,
  error: null,
  contacts: state.contacts.map(contact =>
    contact.id === action.id
      ? { ...contact, name: action.contact.name }
      : contact,
  ),
});

export const editContactFailure = (state, action) => ({
  ...state,
  error: action.error,
});

export default makeReducerCreator(initialState, {
  [ContactTypes.GET_ALL_CONTACT_SUCCESS]: getAllContactSuccess,
  [ContactTypes.GET_ALL_CONTACT_FAILURE]: getAllContactFailure,

  [ContactTypes.ADD_CONTACT_SUCCESS]: addContactSuccess,
  [ContactTypes.ADD_CONTACT_FAILURE]: addContactFailure,

  [ContactTypes.DELETE_CONTACT_SUCCESS]: deleteContactSuccess,
  [ContactTypes.DELETE_CONTACT_FAILURE]: deleteContactFailure,

  [ContactTypes.EDIT_CONTACT_SUCCESS]: editContactSuccess,
  [ContactTypes.EDIT_CONTACT_FAILURE]: editContactFailure,
});
