import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const ContactTypes = makeConstantCreator(
  'GET_ALL_CONTACT',
  'GET_ALL_CONTACT_SUCCESS',
  'GET_ALL_CONTACT_FAILURE',

  'ADD_CONTACT',
  'ADD_CONTACT_SUCCESS',
  'ADD_CONTACT_FAILURE',

  'DELETE_CONTACT',
  'DELETE_CONTACT_SUCCESS',
  'DELETE_CONTACT_FAILURE',

  'GET_NEXT_CONTACTS',
  'GET_PREV_CONTACTS',

  'UNSUBSCRIBE',
  'EDIT_CONTACT',
  'EDIT_CONTACT_SUCCESS',
  'EDIT_CONTACT_FAILURE',
);

export const getAllContactAction = () =>
  makeActionCreator(ContactTypes.GET_ALL_CONTACT);
export const getAllContactSuccessAction = data =>
  makeActionCreator(ContactTypes.GET_ALL_CONTACT_SUCCESS, { data });
export const getAllContactFailureAction = ({ error }) =>
  makeActionCreator(ContactTypes.GET_ALL_CONTACT_FAILURE, { error });

export const addContactAction = ({ contact }) =>
  makeActionCreator(ContactTypes.ADD_CONTACT, { contact });
export const addContactSuccessAction = () =>
  makeActionCreator(ContactTypes.ADD_CONTACT_SUCCESS);
export const addContactFailureAction = ({ error }) =>
  makeActionCreator(ContactTypes.ADD_CONTACT_FAILURE, { error });

export const deleteContactAction = ({ id }) =>
  makeActionCreator(ContactTypes.DELETE_CONTACT, { id });
export const deleteContactSuccessAction = () =>
  makeActionCreator(ContactTypes.DELETE_CONTACT_SUCCESS);
export const deleteContactFailureAction = ({ error }) =>
  makeActionCreator(ContactTypes.DELETE_CONTACT_FAILURE, { error });
export const unSubscribeAction = () =>
  makeActionCreator(ContactTypes.UNSUBSCRIBE);
export const getNextContactsAction = ({ lastContact }) =>
  makeActionCreator(ContactTypes.GET_NEXT_CONTACTS, { lastContact });
export const getPrevContactsAction = ({ firstContact }) =>
  makeActionCreator(ContactTypes.GET_PREV_CONTACTS, { firstContact });
export const editContactAction = ({ id, contact }) =>
  makeActionCreator(ContactTypes.EDIT_CONTACT, { id, contact });
export const editContactSuccessAction = ({ id, contact }) =>
  makeActionCreator(ContactTypes.EDIT_CONTACT_SUCCESS, { id, contact });
export const editContactFailureAction = ({ error }) =>
  makeActionCreator(ContactTypes.EDIT_CONTACT_FAILURE, { error });
