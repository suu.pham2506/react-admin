import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const ModalTypes = makeConstantCreator(
  'SHOW_MODAL',
  'CLOSE_MODAL',
  'SHOW_MODAL_WITH_ITEM',
  'TOGGLE_MODAL',
  'SAVE_DRAFT',
);

export const showModalAction = data =>
  makeActionCreator(ModalTypes.SHOW_MODAL, { data });
export const closeModalAction = id =>
  makeActionCreator(ModalTypes.CLOSE_MODAL, { id });
export const showModalWithItemAction = ({ route, item }) =>
  makeActionCreator(ModalTypes.SHOW_MODAL_WITH_ITEM, { route, item });
export const toggleModalAction = id =>
  makeActionCreator(ModalTypes.TOGGLE_MODAL, { id });
export const saveDraftAction = ({ data, id }) =>
  makeActionCreator(ModalTypes.SAVE_DRAFT, { data, id });
