import { createSelector } from 'reselect';

const modalSelector = (state, props) =>
  state.modal.modals.find(modal => modal.id === props.id);

export const draftSelector = createSelector(
  modalSelector,
  modal => modal && modal.draft,
);

export const itemSelector = createSelector(
  modalSelector,
  modal => modal && modal.item,
);
