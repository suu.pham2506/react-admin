import { createSelector } from 'reselect';

const appointmentsByPageSelector = state =>
  state.appointments.appointmentsByPage;

export const lastAppointmentSelector = createSelector(
  appointmentsByPageSelector,
  appointmentsByPage => (appointmentsByPage[9] ? appointmentsByPage[9] : null),
);

export const firstAppointmentSelector = createSelector(
  appointmentsByPageSelector,
  appointmentsByPage => (appointmentsByPage[0] ? appointmentsByPage[0] : null),
);
