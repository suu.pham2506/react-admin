import { makeReducerCreator } from '../../utils/reduxUtils';
import { AppointmentsTypes } from './actions';

export const initialState = {
  appointmentsByPage: [],
  error: null,
};

const createAppointmentSuccess = state => ({
  ...state,
  error: null,
});

const createAppointmentFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deleteAppointmentSuccess = (state, action) => ({
  ...state,
  error: null,
  appointmentsByPage: state.appointmentsByPage.filter(
    appointment => appointment.id !== action.id,
  ),
});

const deleteAppointmentFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editAppointmentSuccess = (state, action) => ({
  ...state,
  error: null,
  appointmentsByPage: state.appointmentsByPage.map(appointment =>
    appointment.id === action.id
      ? { ...appointment, role: action.appointment.role }
      : appointment,
  ),
});

const editAppointmentFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getAppointmentsSuccess = (state, action) => ({
  ...state,
  appointmentsByPage: action.appointmentsByPage,
});

const getAppointmentsFail = (state, action) => ({
  ...state,
  error: action.error,
});

const appointments = makeReducerCreator(initialState, {
  [AppointmentsTypes.CREATE_APPOINTMENT_FAIL]: createAppointmentFail,
  [AppointmentsTypes.CREATE_APPOINTMENT_SUCCESS]: createAppointmentSuccess,
  [AppointmentsTypes.DELETE_APPOINTMENT_FAIL]: deleteAppointmentFail,
  [AppointmentsTypes.DELETE_APPOINTMENT_SUCCESS]: deleteAppointmentSuccess,
  [AppointmentsTypes.EDIT_APPOINTMENT_FAIL]: editAppointmentFail,
  [AppointmentsTypes.EDIT_APPOINTMENT_SUCCESS]: editAppointmentSuccess,
  [AppointmentsTypes.GET_APPOINTMENTS_FAIL]: getAppointmentsFail,
  [AppointmentsTypes.GET_APPOINTMENTS_SUCCESS]: getAppointmentsSuccess,
});

export default appointments;
