import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux-starter-kit';
import auth from './auth/reducer';
import modal from './modal/reducer';
import previewInterview from './previewInterview/reducer';
import loading from './loading/reducer';
import pagingButtons from './pagingButtons/reducer';
import users from './users/reducer';
import searchBar from './searchBar/reducer';
import managerSource from './managerSource/reducer';
import contact from './contact/reducer';
import appointments from './appointments/reducer';
import positions from './positions/reducer';
import employees from './employees/reducer';
import answers from './answers/reducer';

export default history =>
  combineReducers({
    router: connectRouter(history),
    auth,
    loading,
    modal,
    previewInterview,
    users,
    managerSource,
    contact,
    pagingButtons,
    searchBar,
    appointments,
    positions,
    employees,
    answers,
  });
