import { configureStore } from 'redux-starter-kit';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import reducers from './rootReducers';
import rootSagas from './rootSagas';

export const history = createBrowserHistory();
const enhancers = [];
const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware, routerMiddleware(history)];

const store = configureStore({
  reducer: reducers(history),
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
  enhancers,
});

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./rootReducers', () => store.replaceReducer(reducers));
}

sagaMiddleware.run(rootSagas);

export default store;
