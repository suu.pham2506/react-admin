import { ManagerSourceTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const initialState = {
  managerSources: [],
  managerSource: null,
  error: null,
};

export const getAllManagerSourceSuccess = (state, { data }) => ({
  ...state,
  managerSources: data,
});
export const getManagerSourceByIdSuccess = (state, { data }) => ({
  ...state,
  managerSource: data,
});

export const createSourceSuccess = state => ({
  ...state,
  error: null,
});
export const createSourceFail = (state, action) => ({
  ...state,
  error: action.error,
});

export const deleteSourceSuccess = state => ({
  ...state,
  error: null,
});
export const deleteSourceFali = (state, action) => ({
  ...state,
  error: action.error,
});
export const editSourceSuccess = (state, action) => ({
  ...state,
  error: null,
  managerSources: state.managerSources.map(source =>
    source.id === action.id ? { ...source, type: action.source.type } : source,
  ),
});
export const editSourceFail = (state, action) => ({
  ...state,
  error: action.error,
});
export const getSourcesFail = (state, action) => ({
  ...state,
  error: action.error,
});

export default makeReducerCreator(initialState, {
  [ManagerSourceTypes.GET_ALL_MANAGER_SOURCE_SUCCESS]: getAllManagerSourceSuccess,
  [ManagerSourceTypes.GET_MANAGER_SOURCE_BY_ID_SUCCESS]: getManagerSourceByIdSuccess,

  [ManagerSourceTypes.CREATE_SOURCE_SUCCESS]: createSourceSuccess,
  [ManagerSourceTypes.CREATE_SOURCE_FAIL]: createSourceFail,

  [ManagerSourceTypes.DELETE_SOURCE_SUCCESS]: deleteSourceSuccess,
  [ManagerSourceTypes.DELETE_SOURCE_FAIL]: deleteSourceFali,

  [ManagerSourceTypes.EDIT_SOURCE_SUCCESS]: editSourceSuccess,
  [ManagerSourceTypes.EDIT_SOURCE_FAIL]: editSourceFail,

  [ManagerSourceTypes.GET_SOURCES_FAIL]: getSourcesFail,
});
