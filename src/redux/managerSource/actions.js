import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const ManagerSourceTypes = makeConstantCreator(
  'GET_ALL_MANAGER_SOURCE',
  'GET_ALL_MANAGER_SOURCE_SUCCESS',
  'GET_NEXT_SOURCES',
  'GET_PREV_SOURCES',
  'GET_SOURCES_FAIL',
  'GET_MANAGER_SOURCE_BY_ID',
  'GET_MANAGER_SOURCE_BY_ID_SUCCESS',

  'CREATE_SOURCE',
  'CREATE_SOURCE_SUCCESS',
  'CREATE_SOURCE_FAIL',

  'DELETE_SOURCE',
  'DELETE_SOURCE_SUCCESS',
  'DELETE_SOURCE_FAIL',

  'EDIT_SOURCE',
  'EDIT_SOURCE_SUCCESS',
  'EDIT_SOURCE_FAIL',

  'UNSUBSCRIBE',
);

export const getAllManagerSourceAction = () =>
  makeActionCreator(ManagerSourceTypes.GET_ALL_MANAGER_SOURCE);
export const getManagerSourceById = id =>
  makeActionCreator(ManagerSourceTypes.GET_MANAGER_SOURCE_BY_ID, { id });
export const getAllManagerSourceSuccess = data =>
  makeActionCreator(ManagerSourceTypes.GET_ALL_MANAGER_SOURCE_SUCCESS, {
    data,
  });
export const getManagerSourceByIdSuccsses = data =>
  makeActionCreator(ManagerSourceTypes.GET_MANAGER_SOURCE_BY_ID_SUCCESS, {
    data,
  });

export const createSourceAction = ({ source }) =>
  makeActionCreator(ManagerSourceTypes.CREATE_SOURCE, { source });
export const createSourceSuccessAction = () =>
  makeActionCreator(ManagerSourceTypes.CREATE_SOURCE_SUCCESS);
export const createSourceFailAction = ({ error }) =>
  makeActionCreator(ManagerSourceTypes.CREATE_SOURCE_FAIL, { error });

export const deleteSourceAction = ({ id }) =>
  makeActionCreator(ManagerSourceTypes.DELETE_SOURCE, { id });
export const deleteSourceSuccessAction = ({ id }) =>
  makeActionCreator(ManagerSourceTypes.DELETE_SOURCE_SUCCESS, { id });
export const deleteSourceFailAction = ({ error }) =>
  makeActionCreator(ManagerSourceTypes.DELETE_SOURCE_FAIL, { error });

export const editSourceAction = ({ id, source }) =>
  makeActionCreator(ManagerSourceTypes.EDIT_SOURCE, { id, source });
export const editSourceSuccessAction = ({ id, source }) =>
  makeActionCreator(ManagerSourceTypes.EDIT_SOURCE_SUCCESS, { id, source });
export const editSourceFailAction = ({ error }) =>
  makeActionCreator(ManagerSourceTypes.EDIT_SOURCE_FAIL, { error });
export const unSubscribeAction = () =>
  makeActionCreator(ManagerSourceTypes.UNSUBSCRIBE);
export const getNextSourcesAction = ({ lastSource }) =>
  makeActionCreator(ManagerSourceTypes.GET_NEXT_SOURCES, { lastSource });
export const getPrevSourcesAction = ({ firstSource }) =>
  makeActionCreator(ManagerSourceTypes.GET_PREV_SOURCES, { firstSource });
export const getSourcesFailAction = ({ error }) =>
  makeActionCreator(ManagerSourceTypes.GET_SOURCES_FAIL, { error });
