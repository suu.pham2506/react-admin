import { takeEvery } from 'redux-saga/effects';
import firebase from 'firebase';
import { FirebaseImgTypes } from './actions';

function* uploadImageSaga({ file, onSuccess }) {
  try {
    const storageRef = yield firebase.storage().ref();
    const metadata = {
      contentType: 'image/jpeg',
    };
    const image = yield storageRef.child(file.uid).put(file, metadata);
    yield onSuccess(null, image);
  } catch (e) {
    console.error(e.message);
  }
}

function* getImageUrlSaga({ imgName, setURL }) {
  try {
    const storageRef = yield firebase.storage().ref();
    const imgURL = yield storageRef.child(imgName).getDownloadURL();
    setURL(imgURL);
  } catch (e) {
    console.error(e.message);
  }
}

export default [
  takeEvery(FirebaseImgTypes.UPLOAD_IMAGE, uploadImageSaga),
  takeEvery(FirebaseImgTypes.GET_IMAGE_URL, getImageUrlSaga),
];
