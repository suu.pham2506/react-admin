import styled from 'styled-components';

export default styled.ul`
  background: ${({ theme }) => theme.palette.background[0]};
  padding: 15px;
  list-style-type: none;
  counter-reset: section;
`;
