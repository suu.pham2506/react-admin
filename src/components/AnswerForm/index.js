import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import QuestionAnswer from '../common/QuestionAnswer';
import StyledAnswerPage from './style';

export const dataInput = createRef();

const AnswerPage = ({ answer }) => (
  <StyledAnswerPage ref={dataInput}>
    {answer.questions?.map((question, index) => (
      <QuestionAnswer
        key={question.id}
        question={question.title}
        answer={answer.answers[index]}
      />
    ))}
  </StyledAnswerPage>
);

AnswerPage.propTypes = {
  answer: PropTypes.any,
};

export default AnswerPage;
