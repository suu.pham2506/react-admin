import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import PagingButtons from '../PagingButtons';
import TableWrapper from './style';

const CustomTable = ({
  firstData,
  lastData,
  disableNext,
  disablePrev,
  columns,
  getPreviousData,
  getNextData,
  dataSource,
  rowKey,
}) => (
  <TableWrapper>
    <Table
      pagination={false}
      {...{
        dataSource,
        columns,
        rowKey,
      }}
    />
    <PagingButtons
      {...{
        firstData,
        lastData,
        disableNext,
        disablePrev,
        getNextData,
        getPreviousData,
      }}
    />
  </TableWrapper>
);

CustomTable.propTypes = {
  firstData: PropTypes.object,
  lastData: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  columns: PropTypes.array,
  getPreviousData: PropTypes.func,
  getNextData: PropTypes.func,
  dataSource: PropTypes.array,
  rowKey: PropTypes.func,
};

export default CustomTable;
