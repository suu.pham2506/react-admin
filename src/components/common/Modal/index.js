import React from 'react';
import { Button, Icon } from 'antd';
import i18n from 'i18next';
import PropTypes from 'prop-types';
import HideButton from '../HideButton';
import ModalWrapper from './styles';

const CustomModal = ({
  title,
  onCancel,
  children,
  toggleModal,
  isHidden,
  id,
}) => (
  <ModalWrapper>
    <div className="modalHeader">
      <header>{i18n.t(title)}</header>
      <HideButton {...{ isHidden, toggleModal, id }} />
      <Button onClick={onCancel}>
        <Icon type="close" />
      </Button>
    </div>
    <div className="modalBody">{children}</div>
  </ModalWrapper>
);

CustomModal.propTypes = {
  children: PropTypes.node,
  props: PropTypes.any,
  isHidden: PropTypes.bool,
  toggleModal: PropTypes.func,
  id: PropTypes.number,
  title: PropTypes.string,
  onCancel: PropTypes.func,
};

export default CustomModal;
