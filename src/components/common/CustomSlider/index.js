import React, { useState } from 'react';
import { Menu, Icon, Layout } from 'antd';
import { findLast } from 'utils/arrayUtils';
// eslint-disable-next-line import/no-cycle
import { PRIVATE_ROUTES } from 'routes/PrivateRoutes';
import { HamburgerIcon } from 'assets/icons';
import { history } from 'redux/store';
import StyledSlider from './style';

const { Sider } = Layout;

const CustomSlider = () => {
  const defaultSelectedKeys =
    findLast(
      PRIVATE_ROUTES,
      menu => window.location.pathname.indexOf(menu.path) === 0,
    ) || PRIVATE_ROUTES[0];

  const [collapsed, setCollapsed] = useState(true);

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <StyledSlider>
      <input
        onChange={() => {}}
        id="collapsedTracker"
        type="checkbox"
        checked={!collapsed}
      />
      <label htmlFor="collapsedTracker" className="overlay" onClick={toggle} />
      <aside className="left">
        <Icon className="trigger" component={HamburgerIcon} onClick={toggle} />
        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
          className="sidebar"
          collapsedWidth={64}
          width={180}
        >
          <Menu mode="inline" defaultSelectedKeys={[defaultSelectedKeys.key]}>
            {PRIVATE_ROUTES.map(menu => (
              <Menu.Item key={menu.key} onClick={() => history.push(menu.path)}>
                <Icon component={menu.icon} />
                <span>{menu.text}</span>
              </Menu.Item>
            ))}
          </Menu>
        </Sider>
      </aside>
    </StyledSlider>
  );
};

export default CustomSlider;
