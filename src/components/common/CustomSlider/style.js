import styled from 'styled-components';

export default styled.div`
  overflow: hidden;
  height: auto;
  left: 0;
  background: #fff;
  input#collapsedTracker {
    display: none;
  }
  .sidebar {
    overflow: hidden;
    left: 0;
    background: #fff;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 6px 3px;
    position: fixed;
    z-index: 1000;
    height: 100vh;
    .ant-layout-sider-children {
      display: flex;
      flex-direction: column;
    }
    .ant-menu {
      font-size: 1em;
      width: 180px;
      box-sizing: border-box;
    }
    .ant-menu-inline {
      border-right: 0;
      .ant-menu-item {
        margin-top: 0;
      }
    }
    .ant-menu-inline-collapsed {
      width: 64px;
    }
  }
  .left {
    background: white;
  }
  .ant-layout-sider-children {
    display: flex;
    flex-direction: column;
  }
  .ant-menu-inline-collapsed {
    width: 64px;
  }
  .trigger {
    width: 64px;
    font-size: 20px;
    line-height: 64px;
    cursor: pointer;
    transition: color 0.3s;

    &:hover {
      color: ${({ theme }) => theme.palette.primary};
    }
    @media only screen and (max-width: 430px) {
      color: ${({ theme }) => theme.palette.primary};
    }
  }
  #collapsedTracker {
    width: 0px;
    height: 0px;
    position: absolute;
  }
  @media only screen and (max-width: 430px) {
    .sidebar {
      left: -80px;
      position: fixed;
      z-index: 9999;
    }
    .overlay {
      z-index: 9998;
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      opacity: 0;
      pointer-events: none;
      background: rgba(0, 0, 0, 0.5);
      transition: all 0.5s ease 0s;
    }
    #collapsedTracker:checked ~ .sidebar {
      left: 0px;
    }
    #collapsedTracker:checked ~ .overlay {
      opacity: 1;
      pointer-events: auto;
    }
  }
  .ant-menu-inline-collapsed .ant-menu-item {
    font-size: 1em;
    padding: 0px 0px 0px 23px !important;
    height: 60px;
    margin: 0;
    justify-content: flex-start;
    display: flex;
    align-items: center;
    border: 0;
    .anticon {
      font-size: 16px;
    }
  }
  .ant-menu .ant-menu-item {
    font-size: 1em;
    height: 60px;
    display: flex;
    align-items: center;
    .anticon {
      font-size: 1em;
    }
  }
  .ant-menu-vertical .ant-menu-item:not(:last-child),
  .ant-menu-vertical-left .ant-menu-item:not(:last-child),
  .ant-menu-vertical-right .ant-menu-item:not(:last-child),
  .ant-menu-inline .ant-menu-item:not(:last-child) {
    margin-bottom: 0px;
  }
  .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
    background-color: #198ace;
    color: white;
    svg {
      path {
        fill: white;
      }
    }
  }
  .ant-menu-vertical {
    border-right: 0;
  }
  .ant-menu-vertical .ant-menu-item::after,
  .ant-menu-vertical-left .ant-menu-item::after,
  .ant-menu-vertical-right .ant-menu-item::after,
  .ant-menu-inline .ant-menu-item::after {
    border-right: 0;
  }
`;
