import React from 'react';
import PropTypes from 'prop-types';
import StyledQuestionAnswer from './style';

const QuestionAnswer = ({ question, answer }) => (
  <StyledQuestionAnswer>
    <h3 className="question">{question}</h3>
    <p className="answer">{answer[answer.type]}</p>
  </StyledQuestionAnswer>
);

QuestionAnswer.propTypes = {
  question: PropTypes.string,
  answer: PropTypes.shape({
    type: PropTypes.string,
  }),
};

export default QuestionAnswer;
