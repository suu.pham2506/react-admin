import React from 'react';
import { Icon } from 'antd';
import i18n from 'i18next';
import { useDispatch } from 'react-redux';
import { GoogleIcon } from 'assets/icons';
import { login } from 'redux/auth/reducer';
import StyledButton from './style';

const ButtonGoogle = () => {
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(login());
  };
  return (
    <StyledButton onClick={handleClick}>
      <Icon component={GoogleIcon} />
      {i18n.t('login.google')}
    </StyledButton>
  );
};

export default ButtonGoogle;
