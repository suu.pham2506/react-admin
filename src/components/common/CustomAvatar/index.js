import React, { useEffect, useState } from 'react';
import { Avatar } from 'antd';
import PropTypes from 'prop-types';

const CustomAvatar = ({ getImgURL, imgName }) => {
  const [URL, setURL] = useState(null);
  useEffect(() => {
    getImgURL(imgName, setURL);
  }, [setURL, imgName, getImgURL]);
  return (
    <>
      <Avatar src={URL} />
    </>
  );
};

CustomAvatar.propTypes = {
  getImgURL: PropTypes.func,
  imgName: PropTypes.string,
};

export default CustomAvatar;
