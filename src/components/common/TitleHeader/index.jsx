import React from 'react';
import i18n from 'i18next';
import StyledTitleHeader from './style';

const TitleHeader = () => (
  <StyledTitleHeader> {i18n.t('header.title')} </StyledTitleHeader>
);

export default TitleHeader;
