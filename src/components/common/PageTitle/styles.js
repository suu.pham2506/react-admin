import styled from 'styled-components';

const Barlow = ({ theme }) => theme.fonts.secondaryMedium;

export default styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin: 0;
  transition: padding-left 0.3s ease 0.1s, padding-right 0.3s ease 0.1s,
    position 0 ease 0.3s;
  .extraAction {
    margin-bottom: 0.5em;
    margin-left: 15px;
  }
  h1 {
    margin: 0;
    font-family: ${Barlow};
    font-size: 36px;
    font-weight: 500;
    color: ${({ theme }) => theme.palette.header.primary};
    flex: 1;
    display: flex;
    align-items: center;
    white-space: nowrap;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    &:after {
      content: '';
      width: 100%;
      height: 1px;
      background-color: ${({ theme }) => theme.palette.color[1]};
      display: flex;
      margin-left: 15px;
    }
  }
`;
