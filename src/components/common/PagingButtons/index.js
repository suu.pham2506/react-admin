import React from 'react';
import { Button, Icon } from 'antd';
import PropTypes from 'prop-types';
import PagingButtonsWrapper from './styles';

const PagingButtons = ({
  firstData,
  lastData,
  disableNext,
  disablePrev,
  getPreviousData,
  getNextData,
}) => (
  <PagingButtonsWrapper className="pagingButtons">
    {!firstData ? (
      <span>
        <Button disabled>
          <Icon type="left" />
        </Button>
      </span>
    ) : (
      <span>
        <Button disabled={disablePrev} onClick={getPreviousData}>
          <Icon type="left" />
        </Button>
      </span>
    )}
    {lastData ? (
      <span>
        <Button disabled={disableNext} onClick={getNextData}>
          <Icon type="right" />
        </Button>
      </span>
    ) : (
      <span>
        <Button disabled>
          <Icon type="right" />
        </Button>
      </span>
    )}
  </PagingButtonsWrapper>
);

PagingButtons.propTypes = {
  firstData: PropTypes.object,
  lastData: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  getNextData: PropTypes.func,
  getPreviousData: PropTypes.func,
};

export default PagingButtons;
