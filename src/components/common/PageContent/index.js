import React from 'react';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Button } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useParams, useRouteMatch } from 'react-router-dom';
import { getPDF } from 'utils/pdfUtils';
import { showModalAction, showModalWithItemAction } from 'redux/modal/actions';
import PageTitle from '../PageTitle';
import PageContentWrapper from './styles';

import { dataInput } from '../../AnswerForm';

const PageContent = ({ title, children }) => {
  const location = useLocation();
  const params = useParams();
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const answer = useSelector(state => state.answers.answer);
  const printData = () => {
    Object.keys(answer).length !== 0 && getPDF(dataInput.current, answer.name);
  };
  const showModalCreatePage = () => {
    const routeCreate = `${location.pathname}/create`;
    dispatch(showModalAction(routeCreate));
  };
  const showMoDalCreatePageWithItem = () => {
    const routeCreate = `${match.path}/create`;
    dispatch(showModalWithItemAction({ route: routeCreate, item: match }));
  };

  const BUTTON = {
    '/answers/': () => null,
    '/answers/:id': () => (
      <Button type="primary" onClick={printData}>
        Download
      </Button>
    ),
    Default: () => (
      <Button
        className="button"
        type="primary"
        onClick={params?.id ? showMoDalCreatePageWithItem : showModalCreatePage}
      >
        <span>{i18n.t('button.create')}</span>
      </Button>
    ),
  };
  const CustomButton = BUTTON[match.path] || BUTTON.Default;
  return (
    <PageContentWrapper>
      <header className="page-header">
        <PageTitle>{i18n.t(title)}</PageTitle>
        <CustomButton />
      </header>
      <div className="mainContent">{children}</div>
    </PageContentWrapper>
  );
};

PageContent.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default PageContent;
