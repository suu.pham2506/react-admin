import React from 'react';
import { Icon, Button } from 'antd';
import PropTypes from 'prop-types';
import HideButtonWrapper from './styles';

const HideButton = ({ isHidden, toggleModal, id }) => (
  <HideButtonWrapper>
    <Button onClick={() => toggleModal(id)}>
      {!isHidden ? <Icon type="minus" /> : <Icon type="arrows-alt" />}
    </Button>
  </HideButtonWrapper>
);

HideButton.propTypes = {
  isHidden: PropTypes.bool,
  id: PropTypes.number,
  toggleModal: PropTypes.func,
};

export default HideButton;
