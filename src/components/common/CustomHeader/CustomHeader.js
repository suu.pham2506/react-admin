import React, { useEffect, memo } from 'react';
import { Menu, Dropdown, Avatar } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { logout, getCurrentUser } from 'redux/auth/reducer';
import StyledHeader from './style';

const CustomHeader = () => {
  const { data, roles } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const profileMenu = [
    {
      key: 'profile',
      text: 'Profile',
      url: '#',
    },
  ];
  useEffect(() => {
    dispatch(getCurrentUser());
  }, [dispatch]);
  return (
    <StyledHeader>
      <div className="rightHeader">
        <Dropdown
          trigger={['click']}
          overlay={(
<Menu style={{ minWidth: '120px' }}>
              {profileMenu.map(menu => (
                <Menu.Item key={menu.key}>
                  <a href={menu.url}>{menu.text}</a>
                </Menu.Item>
              ))}
              <Menu.Divider />
              <Menu.Item
                onClick={() => {
                  dispatch(logout());
                }}
                key="logout"
              >
                Logout
              </Menu.Item>
            </Menu>
)}
        >
          <Avatar size="large" src={data?.photoURL} />
        </Dropdown>
        <div className="info">
          <p className="name">{data?.displayName}</p>
          <p className="role">{roles ? 'Admin' : ''}</p>
        </div>
      </div>
    </StyledHeader>
  );
};

CustomHeader.propTypes = {};

export default memo(CustomHeader);
