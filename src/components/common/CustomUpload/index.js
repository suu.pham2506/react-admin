import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Upload, Icon } from 'antd';
import { connect } from 'react-redux';
import { uploadImageAction } from 'redux/firebaseImg/actions';

const CustomUpload = ({
  uploadImage,
  form,
  label,
  src,
  colon,
  required,
  message,
}) => {
  const { getFieldDecorator } = form;

  const handleUpload = ({ onError, onSuccess, file }) => {
    try {
      uploadImage(file, onSuccess);
    } catch (error) {
      onError(error);
    }
  };
  return (
    <Form.Item label={label} colon={colon}>
      {getFieldDecorator(src, {
        rules: [
          {
            required,
            message,
          },
        ],
      })(
        <Upload name="logo" customRequest={handleUpload} listType="picture">
          <Button>
            <Icon type="upload" />
          </Button>
        </Upload>,
      )}
    </Form.Item>
  );
};

CustomUpload.propTypes = {
  uploadImage: PropTypes.func.isRequired,
  form: PropTypes.shape({
    getFieldDecorator: PropTypes.func.isRequired,
    setFieldsValue: PropTypes.func.isRequired,
  }),
  label: PropTypes.string,
  src: PropTypes.string.isRequired,
  colon: PropTypes.bool,
  required: PropTypes.bool,
  message: PropTypes.string,
};

CustomUpload.defaultProps = {
  colon: true,
  required: false,
  message: null,
};

const mapDispatchToProps = dispatch => ({
  uploadImage: (file, onSuccess) =>
    dispatch(uploadImageAction(file, onSuccess)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Form.create()(CustomUpload));
