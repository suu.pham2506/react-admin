import styled from 'styled-components';

export default styled.div`
  width: 100%;
  @media all and (max-width: 768px) {
    height: 100vh;
    overflow-y: scroll;
  }
  /* Custome select */
  .ant-col {
    width: 10em;
  }
  /* Custome select */
  .question {
    padding-top: 6em;
    font-size: 2em;
    color: #3a393d;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 2.19;
    letter-spacing: 0.32px;
    @media all and (max-width: 768px) {
      font-size: 1.5em;
    }
  }
  .ant-form-item {
    display: flex;
    .customInput {
      width: 546px;
      height: 1.9em;
      background-color: transparent;
      color: #c1351f;
      font-size: 1.1875em;
      line-height: 38px;
      resize: none;
      padding: 0px 0px 10px 30px;
      border-width: initial;
      border-style: none;
      border-color: initial;
      border-radius: 0px;
      border-image: initial;
      transition: all 0.2s ease 0s;
      overflow: hidden;
      outline: none;
      @media all and (max-width: 768px) {
        width: 100%;
        font-size: 1em;
        padding: 0px 0px 10px 0px;
      }
    }
    .customInput:focus {
      box-shadow: #c1351f 0px 1px;
    }
    .customInput:hover {
      background-color: transparent;
    }
    .customTextArea {
      display: block;
      width: 546px;
      color: red;
      color: #c1351f;
      background-color: transparent;
      font-size: 1.1875em;
      line-height: 38px;
      overflow-x: hidden;
      overflow-wrap: break-word;
      height: 46px;
      resize: none;
      padding: 0px 0px 0px 30px;
      border-radius: 0px;
      border-width: initial;
      border-style: none;
      border-color: initial;
      border-image: initial;
      transition: all 0.2s ease 0s;
      overflow: hidden;
      @media all and (max-width: 768px) {
        width: 100%;
        font-size: 1em;
        padding: 0px 0px 10px 0px;
      }
    }
    .customTextArea:focus {
      box-shadow: #c1351f 0px 1px;
    }
    .customTextArea:hover {
      background: transparent;
    }
  }
`;
