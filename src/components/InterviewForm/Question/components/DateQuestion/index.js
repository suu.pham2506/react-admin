import React from 'react';
import PropTypes from 'prop-types';
import { Form, DatePicker } from 'antd';

import DateQuestionWrapper from './style';

const TimeQuestion = ({ source, form }) => (
  <DateQuestionWrapper>
    <Form.Item>
      {form.getFieldDecorator(source, {
        rules: [
          {
            required: true,
            message: 'Plese choose your date',
          },
        ],
      })(
        <DatePicker
          className="customDate"
          placeholder="DD/MM/YYYY"
          format="D/M/YYYY"
        />,
      )}
    </Form.Item>
  </DateQuestionWrapper>
);

TimeQuestion.propTypes = {
  source: PropTypes.string.isRequired,
  form: PropTypes.object.isRequired,
};

export default TimeQuestion;
