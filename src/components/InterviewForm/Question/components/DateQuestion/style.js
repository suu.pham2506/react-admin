import styled from 'styled-components';

export default styled.div`
  .ant-calendar-today .ant-calendar-date {
    color: #c1351f !important;
    border-color: #c1351f !important;
  }
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customDate {
      margin-left: 30px;
      width: 15em;
      height: 50px;
      .ant-calendar-picker-clear,
      .ant-calendar-picker-icon {
        top: 32%;
      }
      @media all and (max-width: 768px) {
        font-size: 1em;
        width: 100%;
        margin-left: 0px;
      }
      .ant-input {
        color: #3a393d;
        font-size: 1.1875em;
        @media all and (max-width: 768px) {
          font-size: 1em;
        }
      }
      .ant-input:hover {
        border-color: #c1351f;
        color: #3a393d;
      }
      .ant-input:focus {
        border-color: #c1351f;
      }
    }
  }
`;
