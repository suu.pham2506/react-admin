import styled from 'styled-components';

export default styled.div`
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customTextArea {
      display: block;
      width: 70%;
      margin-bottom: 0.5em;
      color: red;
      color: #3a393d;
      background-color: transparent;
      font-size: 1.1875em;
      line-height: 38px;
      overflow-x: hidden;
      overflow-wrap: break-word;
      height: 46px;
      resize: none;
      padding: 0px 0px 0px 30px;
      border-radius: 0px;
      border-width: initial;
      border-style: none;
      border-color: initial;
      border-image: initial;
      transition: all 0.2s ease 0s;
      overflow: hidden;
      box-shadow: #8080804f 0px 1px;
      @media all and (max-width: 768px) {
        width: 100%;
        font-size: 1.1em;
        padding: 0px 0px 10px 0px;
      }
    }
    .customTextArea:focus {
      box-shadow: #c1351f 0px 1px;
    }
    .customTextArea:hover {
      background: transparent;
    }
  }
`;
