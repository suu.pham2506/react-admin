import React, { useState } from 'react';
import { Form, Radio, Input } from 'antd';
import PropTypes from 'prop-types';
import QuizQuestionWrapper, { RadioWrapper } from './style';

const answers = [
  'ABCDEFGJK',
  'SHSDHSDHSD',
  'SDHSDHSDHF',
  'SDHSDHSDHG',
  'SDHSDHSDHGT',
];

const QuizOneChoiceQuestion = ({ source, form }) => {
  const [valueOther, setValueOther] = useState('');

  const handleValueOtherChange = e => {
    setValueOther(e.target.value);
  };
  return (
    <QuizQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please choose the answer',
            },
          ],
        })(
          <Radio.Group>
            {answers.map((answer, index) => (
              <RadioWrapper
                index={String.fromCharCode(index + 65)}
                key={String.fromCharCode(index + 65)}
              >
                <Radio value={answer}>{answer}</Radio>
              </RadioWrapper>
            ))}
            <RadioWrapper index={String.fromCharCode(answers.length + 65)}>
              <Radio value={valueOther}>
                <Input
                  className="customInput"
                  placeholder="Other Answer"
                  onChange={handleValueOtherChange}
                  value={valueOther}
                />
              </Radio>
            </RadioWrapper>
          </Radio.Group>,
        )}
      </Form.Item>
    </QuizQuestionWrapper>
  );
};

QuizOneChoiceQuestion.propTypes = {
  form: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
};

export default QuizOneChoiceQuestion;
