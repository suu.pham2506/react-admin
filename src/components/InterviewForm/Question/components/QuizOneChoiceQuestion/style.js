import styled from 'styled-components';

export const RadioWrapper = styled.div`
  margin-bottom: 1.25em;
  &:last-child {
    margin-bottom: 0px;
  }
  .ant-radio-inner {
    &::after {
      content: '${props => props.index}';
    }
  }
  .ant-radio-wrapper-checked .ant-radio-inner {
    &::after {
      content: '${props => props.index}';
      color: white !important;
    }
  }
`;

export default styled.div`
  .ant-col {
    width: 100%;
    height: 100%;
  }
  .ant-form-item {
    display: flex;
    .customInput {
      padding: 0px 0px 10px 0px;
      font-size: 1em;
      box-shadow: #8080804f 0px 1px;
      font-weight: normal;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.5;
      letter-spacing: 0.21px;
      color: #3a393d;
    }
    .ant-radio-group {
      margin-left: 30px;
      display: flex;
      flex-direction: column;
      @media all and (max-width: 768px) {
        margin-left: 0px;
        font-size: 1em;
      }
      .ant-radio-wrapper {
        display: flex;
        align-items: center;
        &:last-child {
          margin-bottom: 0px;
        }
        .ant-radio-inner {
          width: 2.5em;
          height: 2.5em;
          border-color: #c1351f;
          position: relative;
          &::after {
            top: 50%;
            transform: translate(-50%, -50%);
            left: 50%;
            background-color: transparent;
            color: #c1351f;
            opacity: 1;
          }
        }
      }
      .ant-radio-wrapper-checked .ant-radio-inner {
        background: #c1351f;
      }
    }
    .ant-radio-wrapper:hover .ant-radio,
    .ant-radio:hover .ant-radio-inner,
    .ant-radio-input:focus + .ant-radio-inner {
      background: #c1351f;
      border-radius: 50%;
    }
  }
  span.ant-radio + * {
    padding-right: 0px;
    padding-left: 0px;
    margin-left: 1.8em;
    font-size: 1em;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: 0.21px;
    color: #3a393d;
  }
`;
