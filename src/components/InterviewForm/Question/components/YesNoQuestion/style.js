import styled from 'styled-components';
import { path, cross } from '../../../../../assets/icons';

export default styled.div`
  .ant-col {
    width: 100%;
    height: 100%;
  }
  .ant-form-item {
    display: flex;
    .ant-radio-group {
      margin-left: 30px;
      display: flex;
      @media all and (max-width: 768px) {
        margin-left: 0px;
        font-size: 1em;
      }
      .ant-radio-wrapper-checked .ant-radio-inner {
        background: #c1351f;
      }
      .ant-radio-wrapper:hover .ant-radio,
      .ant-radio:hover .ant-radio-inner,
      .ant-radio-input:focus + .ant-radio-inner {
        background: #c1351f;
        border-radius: 50%;
      }
      .ant-radio-wrapper:nth-child(1) {
        margin-right: 6em;
        display: flex;
        align-items: center;
        .ant-radio-inner {
          width: 1.875em;
          height: 1.875em;
          border-color: #c1351f;
          position: relative;
          &::after {
            background-image: url(${path});
            background-size: 18px 18px;
            content: '';
            top: 50%;
            transform: translate(-50%, -50%);
            left: 50%;
            background-color: #c1351f;
            width: 18px;
            height: 18px;
          }
        }
      }
      .ant-radio-wrapper:nth-child(2) {
        margin-right: 6em;
        display: flex;
        align-items: center;
        .ant-radio-inner {
          width: 1.875em;
          height: 1.875em;
          border-color: #c1351f;
          position: relative;
          &::after {
            background-image: url(${cross});
            background-size: 18px 18px;
            content: '';
            top: 50%;
            transform: translate(-50%, -50%);
            left: 50%;
            background-color: #c1351f;
            width: 18px;
            height: 18px;
          }
        }
      }
    }
  }
`;
