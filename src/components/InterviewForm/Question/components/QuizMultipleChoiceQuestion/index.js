import React, { useState } from 'react';
import { Form, Checkbox, Input } from 'antd';
import PropTypes from 'prop-types';
import QuizQuestionWrapper, { CheckboxWrapper } from './style';

const answers = [
  'ABCDEFGJK',
  'SHSDHSDHSD',
  'SDHSDHSDHF',
  'SDHSDHSDHG',
  'SDHSDHSDHGT',
];

const QuizMultipleChoiceQuestion = ({ source, form }) => {
  const [valueOther, setValueOther] = useState('');

  const handleValueOtherChange = e => {
    setValueOther(e.target.value);
  };
  return (
    <QuizQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please choose the answer',
            },
          ],
        })(
          <Checkbox.Group>
            {answers.map((answer, index) => (
              <CheckboxWrapper
                index={String.fromCharCode(index + 65)}
                key={String.fromCharCode(index + 65)}
              >
                <Checkbox value={answer}>{answer}</Checkbox>
              </CheckboxWrapper>
            ))}
            <CheckboxWrapper index={String.fromCharCode(answers.length + 65)}>
              <Checkbox value={valueOther}>
                <Input
                  className="customInput"
                  placeholder="Other Answer"
                  onChange={handleValueOtherChange}
                  value={valueOther}
                />
              </Checkbox>
            </CheckboxWrapper>
          </Checkbox.Group>,
        )}
      </Form.Item>
    </QuizQuestionWrapper>
  );
};

QuizMultipleChoiceQuestion.propTypes = {
  form: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
};

export default QuizMultipleChoiceQuestion;
