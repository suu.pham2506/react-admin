import React from 'react';
import { Select, Form } from 'antd';
import PropTypes from 'prop-types';
import SelectQuestionWrapper from './style';

const { Option } = Select;

const SelectQuestion = ({ source, form /* , options */ }) => (
  <SelectQuestionWrapper>
    <Form.Item>
      {form.getFieldDecorator(source, {
        rules: [
          {
            required: true,
            message: 'Please choose the answer',
          },
        ],
      })(
        <Select placeholder="Choose" className="customDropdown">
          {/* {options.map(option => (
              <Option key={option} value={option}>
                {option}
              </Option>
            ))} */}
          <Option value="jack">Jack</Option>
          <Option value="lucy">Lucy</Option>
        </Select>,
      )}
    </Form.Item>
  </SelectQuestionWrapper>
);

SelectQuestion.propTypes = {
  form: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
  // options: PropTypes.array,
};
export default SelectQuestion;
