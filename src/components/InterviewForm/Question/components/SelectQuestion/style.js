import styled from 'styled-components';

export default styled.div`
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customDropdown {
      margin-left: 30px;
      width: 13em;
      height: 50px;
      font-size: 1.1875em;
      @media all and (max-width: 768px) {
        font-size: 1em;
        width: 100%;
        margin-left: 0px;
        padding: 0px 0px 10px 0px;
      }
      .ant-select-selection:hover,
      .ant-select-selection:focus,
      .ant-select-selection:active {
        border-color: #c1351f;
      }
    }
  }
`;
