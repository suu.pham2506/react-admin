import React from 'react';
import PropTypes from 'prop-types';
import QuestionWrapper from './style';
import {
  MutipleQuestion,
  DateQuestion,
  TimeQuestion,
  SelectQuestion,
  OnlineQuestion,
  UploadQuestion,
  RateQuestion,
  YesNoQuestion,
  QuizOneChoiceQuestion,
  QuizMultipleChoiceQuestion,
} from './components';

const Question = ({
  question: { text, type, options },
  form,
  index,
  questionRef,
}) => {
  const QUESTION_TEMPS = {
    'one-line': {
      ContentComponent: OnlineQuestion,
    },
    dropdown: {
      ContentComponent: SelectQuestion,
    },
    time: {
      ContentComponent: TimeQuestion,
    },
    date: {
      ContentComponent: DateQuestion,
    },
    'multiple-line': {
      ContentComponent: MutipleQuestion,
    },
    upload: {
      ContentComponent: UploadQuestion,
    },
    rate: {
      ContentComponent: RateQuestion,
    },
    'yes-no': {
      ContentComponent: YesNoQuestion,
    },
    quiz: {
      ContentComponent: QuizOneChoiceQuestion,
    },
    'quiz-multiple': {
      ContentComponent: QuizMultipleChoiceQuestion,
    },
  };

  const { ContentComponent } = QUESTION_TEMPS[type];
  return (
    <QuestionWrapper ref={questionRef} id={`qs-${index}`}>
      <h1 className="question">
        {index} {text}
      </h1>
      <ContentComponent {...{ form, options }} source={text} />
      <a href={`#qs-${index + 1}`}> Down</a>
    </QuestionWrapper>
  );
};

Question.propTypes = {
  questionRef: PropTypes.object.isRequired,
  question: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
};

export default Question;
