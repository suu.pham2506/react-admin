import styled from 'styled-components';

const fontPrimaryBold = props => props.theme.fonts.primaryBold;
const formPrimaryColor = props => props.theme.palette.formPrimaryColor;

export default styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: center;
  height: 100vh;
  @media all and (max-width: 768px) {
    padding: 1em;
  }
  .logo {
    width: 21em;
    height: auto;
    object-fit: contain;
    margin-bottom: 42px;
  }
  .title {
    @media all and (max-width: 768px) {
      display: flex;
      align-items: center;
      flex-direction: column;
    }
    .interview {
      font-family: ${fontPrimaryBold}, sans-serif;
      -webkit-text-stroke: 2px ${formPrimaryColor};
      font-size: 2.56em;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.46;
      letter-spacing: 2px;
      color: rgba(58, 57, 61, 0);
    }
    .question-form {
      font-family: ${fontPrimaryBold}, sans-serif;
      font-size: 2.56em;
      font-weight: bold;
      font-style: normal;
      font-stretch: normal;
      line-height: 1.46;
      letter-spacing: 2px;
      color: #3a393d;
    }
  }
  .please-answer {
    margin-top: 30px;
    width: 750px;
    height: 90px;
    font-size: 1em;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.88;
    letter-spacing: 0.2px;
    color: #060606;
    @media all and (max-width: 768px) {
      width: 100%;
    }
    .text-center {
      text-align: center;
      margin: 0;
    }
  }
`;
