import React from 'react';
import HeaderWrapper from './style';
import { enouvo } from '../../../assets/icons';

const Header = () => (
  <HeaderWrapper>
    <img src={enouvo} alt="Enouvo" className="logo" />
    <div className="title">
      <span className="interview"> INTERVIEW</span>
      <span className="question-form"> QUESTION FORM </span>
    </div>
    <div className="please-answer">
      <p className="text-center">
        Please answer these questions in English and using your own words. Your
        information will help us to know you better as well as shorten the time
        of recruitment process.
      </p>
      <p className="text-center">
        please send us back soon when it’s finished. Thanks for taking time to
        answer our question!
      </p>
    </div>
    <a type="primary" href="#qs-1">
      Click
    </a>
  </HeaderWrapper>
);

export default Header;
