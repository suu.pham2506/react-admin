import React from 'react';
import { Row, Col } from 'antd';
import i18n from 'i18next';
import {
  enouvo,
  facebook,
  mail,
  instagram,
  behance,
  linkedin,
  location,
  phone,
} from 'assets/icons';
import FooterWrapper from './style';

const Footer = () => (
  <FooterWrapper>
    <Row>
      <Col md={8} xs={24} className="footerLeft">
        <img src={enouvo} alt="Enouvo" className="logo" />
        <ul>
          <li>
            <img src={facebook} alt="Enouvo" />
          </li>
          <li>
            <img src={linkedin} alt="Enouvo" className="linkIcon" />
          </li>
          <li>
            <img src={instagram} alt="Enouvo" />
          </li>
          <li>
            <img src={behance} alt="Enouvo" />
          </li>
        </ul>
      </Col>
      <Col md={8} xs={24} className="footerCenter">
        <p>{i18n.t('footer.aboutUs')}</p>
      </Col>
      <Col md={8} xs={24} className="footerRight">
        <ul>
          <li>
            <img src={location} alt="Enouvo" />
            <span>{i18n.t('footer.mail')}</span>
          </li>
          <li>
            <img src={mail} alt="Enouvo" />
            <span>{i18n.t('footer.location')}</span>
          </li>
          <li>
            <img src={phone} alt="Enouvo" />
            <span>{i18n.t('footer.phone')}</span>
          </li>
        </ul>
      </Col>
    </Row>
    <Row>
      <p className="info">{i18n.t('footer.info')}</p>
    </Row>
  </FooterWrapper>
);

export default Footer;
