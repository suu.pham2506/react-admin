import styled from 'styled-components';

export default styled.footer`
  border-top: 1px solid #e4e4e4;
  padding-top: 105px;
  .info {
    margin-top: 57px;
    text-align: center;
    font-size: 1em;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.88;
    letter-spacing: 0.3px;
    color: #a5a5a5;
  }
  .footerLeft {
    display: flex;
    flex-direction: column;
    justify-content: start;
    align-items: center;
    ul {
      margin-top: 23px;
      padding: 0;
      list-style-type: none;
      display: flex;
      width: 223px;
      justify-content: space-around;
      li {
        width: 42px;
        height: 42px;
        box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.21);
        border: solid 1px #eeeeee;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        img {
          width: 16px;
          height: 16px;
          object-fit: contain;
        }
        .linkIcon {
          width: 42px;
          height: 42px;
          object-fit: contain;
        }
      }
    }
    .logo {
      width: 211px;
      height: 70px;
      object-fit: contain;
    }
  }
  .footerCenter {
    font-size: 1em;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.88;
    letter-spacing: 0.3px;
    color: #474b68;
    @media all and (max-width: 768px) {
      text-align: center;
    }
  }
  .footerRight {
    display: flex;
    justify-content: center;
    ul {
      padding: 0;
      list-style: none;
      li {
        padding-bottom: 1em;
        display: flex;
        img {
          margin-right: 1.1em;
        }
        span {
          display: inline-block;
          width: 11.375em;
          font-size: 1em;
          font-weight: normal;
          font-style: normal;
          font-stretch: normal;
          line-height: 1.88;
          letter-spacing: 0.27px;
          color: #474b68;
        }
      }
    }
  }
`;
