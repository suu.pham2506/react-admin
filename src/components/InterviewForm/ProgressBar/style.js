import styled from 'styled-components';

export default styled.div`
  position: sticky;
  bottom: 0;
  left: 0;
  padding-top: 4em;
  width: 100%;
  @media all and (max-width: 768px) {
    width: 100%;
    padding-top: 2em;
  }
  .ant-progress-line {
    width: 100%;
    height: 80px;
    font-size: 0.875em;
    background: #080808;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: all 200ms ease-in-out;
    div {
      width: 68%;
      .ant-progress-outer {
        padding: 0;
        margin: 0;
        width: 53%;
        margin-left: 30px;
        @media all and (max-width: 768px) {
          margin-left: 0;
        }
        .ant-progress-inner {
          width: 100%;
          .ant-progress-bg {
            background-color: #c1351f;
          }
        }
      }
      .ant-progress-text {
        display: inline;
        margin-left: 1.5em;
        white-space: nowrap;
        text-align: left;
        vertical-align: middle;
        word-break: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: 0.78px;
        color: #ffffff;
        width: 141px;
        height: 20px;
        font-size: 1em;
        font-weight: 600;
      }
    }
  }
  .ant-progress-status-success
    > div
    > .ant-progress-outer
    > .ant-progress-inner
    > .ant-progress-bg {
    background-color: #52c41a;
  }
`;
