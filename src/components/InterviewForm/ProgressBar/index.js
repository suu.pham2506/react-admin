import React from 'react';
import { Progress } from 'antd';
import ProgressWrapper from './style';

const ProgressBar = props => (
  <ProgressWrapper>
    <Progress {...props} />
  </ProgressWrapper>
);

ProgressBar.propTypes = {};

export default ProgressBar;
