import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Link } from 'react-router-dom';
import { Icon, Button } from 'antd';

import { ExcelButtonIcon } from 'assets/icons';
import CustomTable from '../../common/CustomTable';
import { StyledActionGroup } from './style';

const AnswerTable = ({
  answersByPage,
  deleteAnswer,
  getDefaultAnswers,
  getPreviousAnswers,
  getNextAnswers,
  lastAnswer,
  firstAnswer,
  disableNext,
  disablePrev,
  unSubscribe,
}) => {
  useEffect(() => {
    getDefaultAnswers();
    return () => {
      unSubscribe();
    };
  }, [getDefaultAnswers, unSubscribe]);
  const handleDelete = record => {
    if (answersByPage.length === 1) getPreviousAnswers({ firstAnswer });
    deleteAnswer({ id: record.id });
  };
  const actionGroup = row => (
    <StyledActionGroup>
      <Link to={`/answers/${row.id}`}>
        <Button>
          <Icon component={ExcelButtonIcon} />
        </Button>
      </Link>
      <Button type="primary" onClick={() => handleDelete(row)}>
        {i18n.t('button.delete')}
      </Button>
    </StyledActionGroup>
  );
  const columns = [
    {
      title: i18n.t('table.title.name'),
      dataIndex: 'name',
      key: 'name',
      render: (text, row) => <Link to={`/answers/${row.id}`}>{text}</Link>,
    },
    {
      title: i18n.t('table.title.createdAt'),
      dataIndex: 'created',
      key: 'created',
      render: created => <span>{new Date(created).toLocaleDateString()}</span>,
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => actionGroup(record),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstAnswer}
        lastData={lastAnswer}
        {...{
          disableNext,
          disablePrev,
          columns,
        }}
        getPreviousData={() => getPreviousAnswers({ firstAnswer })}
        getNextData={() => getNextAnswers({ lastAnswer })}
        dataSource={answersByPage}
      />
    </>
  );
};

AnswerTable.propTypes = {
  answersByPage: PropTypes.array,
  deleteAnswer: PropTypes.func,
  getDefaultAnswers: PropTypes.func,
  getPreviousAnswers: PropTypes.func,
  getNextAnswers: PropTypes.func,
  lastAnswer: PropTypes.object,
  firstAnswer: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  unSubscribe: PropTypes.func,
};

export default AnswerTable;
