const functions = require('firebase-functions');
const admin = require('firebase-admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;
const algoliasearch = require('algoliasearch');

admin.initializeApp();

const ALGOLIA_ID = functions.config().algolia.app_id;
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key;
const ALGOLIA_INDEX_NAME = 'iam';
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

exports.onSignUp = functions.auth.user().onCreate(user => {
    if(user.email) {
        return admin.firestore().collection('iam').where('email', '==', user.email).get()
            .then(snapShot => {
                if(!snapShot.empty){ 
                    const claims = {
                        admin: true
                    }
                    admin.auth().setCustomUserClaims(user.uid, claims)
                    admin.firestore().collection('iam').doc(snapShot.docs[0].id).update({user: {
                        uid: user.uid,
                        name: user.name ? user.name : null,
                        photoURL: user.photoURL ? user.photoURL : null,
                        email: user.email ? user.email : null,
                        providerId: user.providerId ? user.providerId : null
                    }})

                    return admin.firestore().collection('metadata').doc(user.uid).set({refreshTime: new Date().getTime()}, {merge: true})
                }
                return null
            })
    }
    return null
})

exports.onDeleteUser = functions.auth.user().onDelete(user => {
    return admin.firestore().collection('iam').where('user.uid', '==', user.uid).get()
        .then(snapShot => {
            if(!snapShot.empty){
                admin.firestore().collection('iam').doc(snapShot.docs[0].id).update({
                    user: FieldValue.delete()
                })
            }
            return admin.firestore().collection('metadata').doc(user.uid).delete()
        })
})

exports.onIamCreated = functions.firestore.document('iam/{userId}').onCreate((snap, context) => {
    const user = snap.data()
    const claims = {
        admin: true
    }
    user.objectID = context.params.userId;
    const index = client.initIndex(ALGOLIA_INDEX_NAME);
    index.saveObject(user);
    return admin.auth().getUserByEmail(user.email)
        .then(u => {
            admin.auth().setCustomUserClaims(u.uid, claims)
            return admin.firestore().collection('iam').doc(snap.id).update({user: {
                uid: u.uid,
                name: u.name ? u.name : null,
                photoURL: u.photoURL ? u.photoURL : null,
                email: u.email ? u.email : null,
                providerId: u.providerId ? u.providerId : null,
            }})
        })
        .catch(error => {
            return null
        })
})

exports.onIamDeleted = functions.firestore.document('iam/{userId}').onDelete((snap, context) => {
    const user = snap.data()
    const claims = {
        admin: false
    }
    const objectID = context.params.userId;
    const index = client.initIndex(ALGOLIA_INDEX_NAME);
    index.deleteObject(objectID);
    return admin.auth().getUserByEmail(user.email)
        .then(u => {
            return admin.auth().setCustomUserClaims(u.uid, claims)
        })
        .catch(error => {
            return null
        })
})

exports.onRetriveTypeFormAnswer = functions.https.onRequest((req, res) => {
    let body = req.body
    if(body.form_response.form_id === 'KilO2J') {
        admin.firestore().collection('answers').doc(body.event_id).set({
            part1: JSON.stringify(body),
            created: new Date().getTime()
        }).then().catch()
    } else {
        admin.firestore().collection('answers').orderBy('created', 'desc').limit(1).get()
            .then(snapShot => {
                return admin.firestore().collection('answers').doc(snapShot.docs[0].id).update({
                    part2: JSON.stringify(body)
                })
            })
            .catch()
    }
    res.send("OK")
})